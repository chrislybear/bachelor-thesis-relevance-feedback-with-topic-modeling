package ch.uzh.topicmodel;

import static org.junit.Assert.*;
import java.io.IOException;
import org.junit.Test;

public class TopicModelTest {
	
	private static final String SAMPLE_FILE = "sample-data/ap.txt";

	@Test
	public void testRunParallelLDA() throws IOException {
		TMParallelLDA tm = new TMParallelLDA(SAMPLE_FILE);
		int seed = 42; // fixing the seed will give deterministic results (i.e. same results over several runs)
		tm.runEstimation(10, 1.0, 0.01, 50, "/Users/chris/Desktop/test_TM_states/", seed);
	}
	
//	@Test
	// Results between ParallelLDA and SimpleLDA are not identical (even with same parameters)!
	public void testRunSimpleLDA() throws IOException {
		TMSimpleLDA tm = new TMSimpleLDA(SAMPLE_FILE);
		int seed = 42; // fixing the seed will give deterministic results (i.e. same results over several runs)
		tm.runEstimation(10, 1.0, 0.01, 50, seed);
	}

}
