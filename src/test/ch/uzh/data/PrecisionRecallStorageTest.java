package ch.uzh.data;

import java.util.Map.Entry;

import org.junit.Test;
import ch.uzh.data.PrecisionRecallStorage.PrecisionRecallPair;

public class PrecisionRecallStorageTest {

	private PrecisionRecallStorage myStorage;
	
	@Test
	public void testEverything() {
		
		myStorage = new PrecisionRecallStorage();
		
		myStorage.add(1, 1.1f, 11.11f);
		myStorage.add(2, 2.2f, 22.22f);
		myStorage.add(3, 3.3f, 33.33f);
		myStorage.add(4, 4.4f, 44.44f);
		myStorage.add(5, 5.5f, 55.55f);
		// add duplicate
		myStorage.add(3, 6.6f, 66.66f);
		// add element with irregular k
		myStorage.add(10, 9.99f, 99.99f);

		System.out.println("\nforeach:");
		for (Entry<Integer, PrecisionRecallPair<Float, Float>> entry : myStorage) {
			int k = entry.getKey();
			System.out.println(k + " " + entry.getValue().getRecall() + " " + entry.getValue().getPrecision());
		}
		
		System.out.println("\nloop with 'at(k)':");
		for (int i = 1; i <= myStorage.size(); i++) {
			PrecisionRecallPair<Float, Float> pair = myStorage.at(i);
			if (pair != null) {
				System.out.println(pair.getRecall() + " " + pair.getPrecision());
			} else {
				System.out.println("Element with k = " + i + " does not exist! There must be an element with an irregular index k. Try 'foreach'.");
			}
		}
		
		System.out.println("\ntoString:");
		System.out.println(myStorage.toString());
	}

}
