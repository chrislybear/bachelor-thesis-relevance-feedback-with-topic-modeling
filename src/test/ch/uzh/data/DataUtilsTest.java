package ch.uzh.data;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import ch.uzh.data.IndexDocument;
import ch.uzh.exceptions.InvalidRelevanceException;

import static ch.uzh.main.Config.*;

public class DataUtilsTest {

//	@Test
	public void testGetDocsFromXml() {
		List<IndexDocument> docs = DataUtils.getDocsFromXml(GREEN_RAW_DATA);
		assertTrue(docs.size() == 13409);
	}
	
//	@Test
	public void processXml() throws IOException {
		DataUtils.processXml(BMW_RAW_DATA, BMW_PROCESSED_DATA);
	}
	
//	@Test
	public void shuffleProcessedData() throws IOException {
		DataUtils.shuffleProcessedData(BMW_PROCESSED_DATA_CLEAN, BMW_PROCESSED_DATA_CLEAN.substring(0, BMW_PROCESSED_DATA_CLEAN.length()-4) + "_shuffled.txt");
	}
	
//	@Test
	public void checkGoldstandard() throws IOException {
		assertTrue(100 == DataUtils.checkGoldstandard("real-data/data-gruen/goldstandard_green_nature.txt"));
	}
	
//	@Test
	public void testGetTotalRelevant() throws IOException, InvalidRelevanceException {
	    Set<Integer> set = new HashSet<Integer>();
	    
	    for (int i = 1; i <= 10; i++) {set.add(i);}
		int totalRelevant = DataUtils.getTotalRelevant(GOLDSTANDARD_GREEN_POLITICS, set);
		assertEquals(4, totalRelevant);
		
		set.clear();
		for (int i = 11; i <= 20; i++) {set.add(i);}
		totalRelevant = DataUtils.getTotalRelevant(GOLDSTANDARD_GREEN_POLITICS, set);
		assertEquals(3, totalRelevant);
		
		set.clear();
		for (int i = 21; i <= 30; i++) {set.add(i);}
		totalRelevant = DataUtils.getTotalRelevant(GOLDSTANDARD_GREEN_POLITICS, set);
		assertEquals(5, totalRelevant);
		
		set.clear();
		for (int i = 31; i <= 40; i++) {set.add(i);}
		totalRelevant = DataUtils.getTotalRelevant(GOLDSTANDARD_GREEN_POLITICS, set);
		assertEquals(3, totalRelevant);
		
		set.clear();
		for (int i = 41; i <= 50; i++) {set.add(i);}
		totalRelevant = DataUtils.getTotalRelevant(GOLDSTANDARD_GREEN_POLITICS, set);
		assertEquals(3, totalRelevant);
		
		set.clear();
		for (int i = 51; i <= 60; i++) {set.add(i);}
		totalRelevant = DataUtils.getTotalRelevant(GOLDSTANDARD_GREEN_POLITICS, set);
		assertEquals(4, totalRelevant);
		
		set.clear();
		for (int i = 61; i <= 70; i++) {set.add(i);}
		totalRelevant = DataUtils.getTotalRelevant(GOLDSTANDARD_GREEN_POLITICS, set);
		assertEquals(3, totalRelevant);
		
		set.clear();
		for (int i = 71; i <= 80; i++) {set.add(i);}
		totalRelevant = DataUtils.getTotalRelevant(GOLDSTANDARD_GREEN_POLITICS, set);
		assertEquals(3, totalRelevant);
		
		set.clear();
		for (int i = 81; i <= 90; i++) {set.add(i);}
		totalRelevant = DataUtils.getTotalRelevant(GOLDSTANDARD_GREEN_POLITICS, set);
		assertEquals(3, totalRelevant);
		
		set.clear();
		for (int i = 91; i <= 100; i++) {set.add(i);}
		totalRelevant = DataUtils.getTotalRelevant(GOLDSTANDARD_GREEN_POLITICS, set);
		assertEquals(5, totalRelevant);
	}
	
//	@Test
	public void testGetAllTotalRelevant() throws IOException, InvalidRelevanceException {
		System.out.println("Total relevant documents in goldstandard");
		System.out.println("green-nature:     " + DataUtils.getTotalRelevant(GOLDSTANDARD_GREEN_NATURE));
		System.out.println("green-politics:   " + DataUtils.getTotalRelevant(GOLDSTANDARD_GREEN_POLITICS));
		System.out.println("bmw-organization: " + DataUtils.getTotalRelevant(GOLDSTANDARD_BMW_ORGANIZATION));
		System.out.println("bmw-car:          " + DataUtils.getTotalRelevant(GOLDSTANDARD_BMW_CAR));
	}
	
	@Test
	public void testGetOverlap() throws IOException, InvalidRelevanceException {
		assertEquals(
				DataUtils.getOverlap(GOLDSTANDARD_GREEN_NATURE, GOLDSTANDARD_GREEN_POLITICS),
				DataUtils.getOverlap(GOLDSTANDARD_GREEN_POLITICS, GOLDSTANDARD_GREEN_NATURE)
				);
		assertEquals(
				DataUtils.getOverlap(GOLDSTANDARD_BMW_ORGANIZATION, GOLDSTANDARD_BMW_CAR),
				DataUtils.getOverlap(GOLDSTANDARD_BMW_CAR, GOLDSTANDARD_BMW_ORGANIZATION)
				);
		System.out.println("Goldstandard overlap");
		System.out.println("green: " + DataUtils.getOverlap(GOLDSTANDARD_GREEN_NATURE, GOLDSTANDARD_GREEN_POLITICS));
		System.out.println("bmw:   " + DataUtils.getOverlap(GOLDSTANDARD_BMW_ORGANIZATION, GOLDSTANDARD_BMW_CAR));
	}

}
