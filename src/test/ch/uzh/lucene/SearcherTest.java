package ch.uzh.lucene;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.queryparser.classic.QueryParserBase;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.junit.Test;

import ch.uzh.data.ExpandableTerm;

import static ch.uzh.main.Config.*;

public class SearcherTest {

//	private static final String SEARCH_STRING = "kollision^2.0 frauen zeugen";
//	private static final String SEARCH_STRING = "Wikifolio^2.5 kollision the der";
	private static final String SEARCH_STRING = "wikifolio kollision mittwochabend zeugen";
//	private static final String SEARCH_STRING = "coupé km/h";
	
//	@Test
	public void testQueryParserEscape() {
		System.out.println(QueryParserBase.escape(SEARCH_STRING));
	}
	
	@Test
	// Note: Filesystem Index has to be created first under ..._INDEX_PATH
	public void testSearch() throws Exception {
		System.err.println("Testing searching on disk index...");
		File fsIndexFile = new File(BMW_INDEX_TM_BASEPATH + "Index_Base");
//		File fsIndexFile = new File(BMW_INDEX_TM_BASEPATH + "Index_BoostEntireContent");
//		File fsIndexFile = new File(BMW_INDEX_TM_BASEPATH + "Index_BoostHighlights");
		if (!fsIndexFile.exists() || !fsIndexFile.isDirectory()) {
			throw new IOException(fsIndexFile + " does not exist or is not a directory.");
		}
		Directory fsIndexDir = FSDirectory.open(fsIndexFile.toPath());
		List<ExpandableTerm> boosts = new ArrayList<ExpandableTerm>();
		
		// doing this makes lucene use custom similarity (MyScoreQuery)
		// does not show in explainScore!!! 
		boosts.add(new ExpandableTerm("grün", 2.0d));
		boosts.add(new ExpandableTerm("mittwochabend", 3.0d));
		boosts.add(new ExpandableTerm("the", 1.5d));
		
//		Searcher.search(fsIndexDir, SEARCH_STRING, 1, true, true, boosts);
		Searcher.search(fsIndexDir, SEARCH_STRING, 1, true, true, null);
	}

}
