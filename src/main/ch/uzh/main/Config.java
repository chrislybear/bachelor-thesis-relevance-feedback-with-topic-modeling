package ch.uzh.main;

public final class Config {
	
	/*
	 * PARAMETERS THAT CHANGE THE OUTCOME OF THE EXPERIMENT
	 */
	
	// Recommended Java version. Simply used to show a warning if a divergent version is used to run the experiments.
	// Other versions than this one might produce different topic models and different index/search behavior (and thus different results).
	public static final String DEFAULT_JAVA = "1.8.0_91";
	
	// number of threads to use for training of the topic model
	public static final int NUM_THREADS = 4;
	
	// stopwords used to create topic model as well as for indexing and searching
	public static final String STOPWORDS_DE_FILE = "stoplists/de.txt";
	
	// parameters for training of the topic model
	public static final int NUM_TOPICS = 100;
	public static final double ALPHA_SUM = 1.0;
	public static final double BETA = 0.01;
	public static final int NUM_ITERATIONS = 50;
	public static final int TM_SEED = 42;
	
	// query string
	public static final String GREEN_BASE_SEARCH_STRING = "grün*";
	public static final String GREEN_POLITICS_EXPANDED_SEARCH_STRING = "grün* politik partei";
	public static final String GREEN_NATURE_EXPANDED_SEARCH_STRING = "grün* natur";
	public static final String BMW_BASE_SEARCH_STRING = "bmw";
	public static final String BMW_ORGANIZATION_EXPANDED_SEARCH_STRING = "bmw konzern firma unternehmen";
	public static final String BMW_CAR_EXPANDED_SEARCH_STRING = "bmw auto fahrzeug modell";
	
	// number of terms retrieved from topic model for query expansion
	public static final int NUM_TERMS = 50;
	
	// top documents to retrieve from Lucene search, should not exceed number of documents in goldstandard
	public static final int NUMBER_OF_SEARCH_RESULTS = 100;
	
	// goldstandard for query "grün"
	public static final String GOLDSTANDARD_GREEN_POLITICS = "real-data/data-gruen/goldstandard_green_politics.txt";
	public static final String GOLDSTANDARD_GREEN_NATURE = "real-data/data-gruen/goldstandard_green_nature.txt";
	public static final String GOLDSTANDARD_BMW_ORGANIZATION = "real-data/data-bmw/goldstandard_bmw_organization.txt";
	public static final String GOLDSTANDARD_BMW_CAR = "real-data/data-bmw/goldstandard_bmw_car.txt";
	
	// Goldstandard has to be split into a part for relevance feedback and a part for evaluation.
	// A split of 5 uses 1/5 (i.e. 20 documents) for relevance feedback and the rest (80 documents) for evaluation.
	// 5 also means that there will be 5 runs for each experiment (different 20 documents every time).
	public static final int GOLDSTANDARD_SPLIT = 5;
	
	// for precision at k
	public static final int PAK = 10;
	
	// Constant boost factor for document boost (experiments a1-b1 and a3-b1).
	// Amplifies the topic distribution of a document.
	// Set to 1 for no amplification.
	public static final double TOPIC_AMPLIFIER = 100;
	
	
	
	/*
	 * DATA FILES
	 */
	
	// data file to be examined, processed clean data is what's used for building the topic model and the index
	public static final String GREEN_RAW_DATA = "real-data/data-gruen/ChrisGruen20160523.xml";
	public static final String GREEN_PROCESSED_DATA = "real-data/data-gruen/ChrisGruen20160523_processed.txt";
	public static final String GREEN_PROCESSED_DATA_CLEAN = "real-data/data-gruen/ChrisGruen20160523_processed_clean.txt";
	public static final String GREEN_REMOVED_DATA = "real-data/data-gruen/ChrisGruen20160523_processed_removed.txt";
	public static final String BMW_RAW_DATA = "real-data/data-bmw/ChrisBMW20160624.xml";
	public static final String BMW_PROCESSED_DATA = "real-data/data-bmw/ChrisBMW20160624_processed.txt";
	public static final String BMW_PROCESSED_DATA_CLEAN = "real-data/data-bmw/ChrisBMW20160624_processed_clean.txt";
	public static final String BMW_REMOVED_DATA = "real-data/data-bmw/ChrisBMW20160624_processed_removed.txt";
	
	
	
	/*
	 * OTHER PARAMETERS
	 */
	
	// where index and topic model states are stored
	public static final String GREEN_INDEX_TM_BASEPATH = "index-tm-state/gruen/";
	public static final String BMW_INDEX_TM_BASEPATH = "index-tm-state/bmw/";
	public static final String TM_STATE_FILENAME = "tm_state";
	
	// where the results for query should be stored
	public static final String RESULTS_OUTPUT_DIR_GREEN_POLITICS =			"real-data/results/green-politics/";
	public static final String RESULTS_OUTPUT_DIR_GREEN_NATURE =			"real-data/results/green-nature/";
	public static final String RESULTS_OUTPUT_DIR_BMW_ORGANIZATION =		"real-data/results/bmw-organization/";
	public static final String RESULTS_OUTPUT_DIR_BMW_CAR =					"real-data/results/bmw-car/";
	
	// field under which the documents content is indexed
	public static final String CONTENT_FIELD = "content";
	// field under which the documents identifier is indexed
	public static final String DOCID_FIELD = "docID";
	
}
