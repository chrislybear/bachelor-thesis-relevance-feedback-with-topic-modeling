package ch.uzh.main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import ch.uzh.data.AssessableDocument;
import ch.uzh.data.DataUtils;
import ch.uzh.data.ExpandableTerm;
import ch.uzh.data.PrecisionRecallStorage;
import ch.uzh.data.ResultDocument;
import ch.uzh.data.PrecisionRecallStorage.PrecisionRecallPair;
import ch.uzh.exceptions.InvalidRelevanceException;
import ch.uzh.exceptions.NoMatchingRecallValueException;
import ch.uzh.lucene.Indexer;
import ch.uzh.lucene.Searcher;
import ch.uzh.topicmodel.TMParallelLDA;

import static ch.uzh.main.Config.*;

public class Main {
	
	private static final Boolean showVerboseIndexing = false;
	private static final Boolean showVerboseTopicTraining = false;
	private static final Boolean showVerboseExperiments = false;
	private static final Boolean showVerboseResults = false;
	private static final Boolean showVerboseStoring = false;

	public static void main(String[] args) throws Exception {
		
		// check Java version
		String systemVersion = System.getProperty("java.version");
		System.out.println("Running Java " + systemVersion);
		if (!systemVersion.equals(DEFAULT_JAVA)) {
			System.out.println("WARNING: Using a different Java version might give different results. Recommended version: " + DEFAULT_JAVA);
		}
		
//		processAndCleanData(GREEN_BASE_SEARCH_STRING, GREEN_RAW_DATA, GREEN_PROCESSED_DATA, GREEN_PROCESSED_DATA_CLEAN, GREEN_REMOVED_DATA);
//		processAndCleanData(BMW_BASE_SEARCH_STRING, BMW_RAW_DATA, BMW_PROCESSED_DATA, BMW_PROCESSED_DATA_CLEAN, BMW_REMOVED_DATA);

		// show strange characters
//		DataUtils.printUnicodeChars(GREEN_PROCESSED_DATA_CLEAN, 78, 24725, 24737);
		
		// grün - Natur
		System.out.println("\nStarting experiment green-nature.");
		runExperiment(GREEN_BASE_SEARCH_STRING,
				GREEN_NATURE_EXPANDED_SEARCH_STRING,
				GREEN_PROCESSED_DATA_CLEAN,
				GREEN_INDEX_TM_BASEPATH,
				GOLDSTANDARD_GREEN_NATURE,
				RESULTS_OUTPUT_DIR_GREEN_NATURE,
				"green-nature");
		
		// grün - Politik
		System.out.println("\nStarting experiment green-politics.");
		runExperiment(GREEN_BASE_SEARCH_STRING,
				GREEN_POLITICS_EXPANDED_SEARCH_STRING,
				GREEN_PROCESSED_DATA_CLEAN,
				GREEN_INDEX_TM_BASEPATH,
				GOLDSTANDARD_GREEN_POLITICS,
				RESULTS_OUTPUT_DIR_GREEN_POLITICS,
				"green-politics");
		
		// BMW - Organization
		System.out.println("\nStarting experiment bmw-organization.");
		runExperiment(BMW_BASE_SEARCH_STRING,
				BMW_ORGANIZATION_EXPANDED_SEARCH_STRING,
				BMW_PROCESSED_DATA_CLEAN,
				BMW_INDEX_TM_BASEPATH,
				GOLDSTANDARD_BMW_ORGANIZATION,
				RESULTS_OUTPUT_DIR_BMW_ORGANIZATION,
				"bmw-organization");
		
		// BMW - Car
		System.out.println("\nStarting experiment bmw-car.");
		runExperiment(BMW_BASE_SEARCH_STRING,
				BMW_CAR_EXPANDED_SEARCH_STRING,
				BMW_PROCESSED_DATA_CLEAN,
				BMW_INDEX_TM_BASEPATH,
				GOLDSTANDARD_BMW_CAR,
				RESULTS_OUTPUT_DIR_BMW_CAR,
				"bmw-car");
		
	}
	
	private static void processAndCleanData(String searchString, String rawDataInfile, String processedDataOutfile, String processedCleanedDataOutfile, String removedDataOutfile) throws IOException, Exception {
		// prepare data for topic model
		DataUtils.processXml(rawDataInfile, processedDataOutfile);
		
		// clean data to contain only documents that match our query
		DataUtils.removeNoHitDocsFromData(new File(processedDataOutfile), searchString, new File(processedCleanedDataOutfile), new File(removedDataOutfile));
	}

	private static void runExperiment(
			String baseSearchString,
			String expandedSearchString,
			String processedCleanedDataInfile,
			String indexAndTopicModelDir,
			String goldstandard,
			String resultsOutputPath,
			String queryAspectID
			) throws UnsupportedEncodingException, FileNotFoundException, IOException, InvalidRelevanceException, Exception {
		
		String indexPathBase =					indexAndTopicModelDir + "Index_Base";
		String indexPathBoostHighlights =		indexAndTopicModelDir + "Index_BoostHighlights";
		String indexPathBoostEntireContent =	indexAndTopicModelDir + "Index_BoostEntireContent";
		String tmStatePath =					indexAndTopicModelDir + "TM_State";
		
		String resultFileBaseline =				resultsOutputPath + "PR-" + queryAspectID + "-baseline.csv";
		String resultFileManualExpansion =		resultsOutputPath + "PR-" + queryAspectID + "-manual_expansion.csv";
		String resultFile_a1_b1 =				resultsOutputPath + "PR-" + queryAspectID + "-a1_b1.csv";
		String resultFile_a1_b2_1 =				resultsOutputPath + "PR-" + queryAspectID + "-a1_b2_1.csv";
		String resultFile_a1_b2_2 =				resultsOutputPath + "PR-" + queryAspectID + "-a1_b2_2.csv";
		String resultFile_a3_b1 =				resultsOutputPath + "PR-" + queryAspectID + "-a3_b1.csv";
		String resultFile_a3_b2_1 =				resultsOutputPath + "PR-" + queryAspectID + "-a3_b2_1.csv";
		String resultFile_a3_b2_2 =				resultsOutputPath + "PR-" + queryAspectID + "-a3_b2_2.csv";
		
		// don't show output
		PrintStream sysout = System.out;
		PrintStream syserr = System.err;
		if (!showVerboseTopicTraining) {
			System.out.println("Building topic model...");
			System.setOut(new NullPrintStream());
			System.setErr(new NullPrintStream());
		}
		
		System.out.println("-----------------------");
		System.out.println(" BUILDING TOPIC MODEL");
		System.out.println("-----------------------");
		
		// create topic model from data
		TMParallelLDA topicModel = new TMParallelLDA(processedCleanedDataInfile);
		
		// train topic model
		topicModel.runEstimation(NUM_TOPICS, ALPHA_SUM, BETA, NUM_ITERATIONS, tmStatePath, TM_SEED);
		
		// show output again
		System.setOut(sysout);
		System.setErr(syserr);
		
		// to store precision at k of every run
		List<Float> PAK_baseline_list = new ArrayList<Float>();
		List<Float> PAK_manualExpansion_list = new ArrayList<Float>();
		List<Float> PAK_a1_b1_list = new ArrayList<Float>();
		List<Float> PAK_a1_b2_1_list = new ArrayList<Float>();
		List<Float> PAK_a1_b2_2_list = new ArrayList<Float>();
		List<Float> PAK_a3_b1_list = new ArrayList<Float>();
		List<Float> PAK_a3_b2_1_list = new ArrayList<Float>();
		List<Float> PAK_a3_b2_2_list = new ArrayList<Float>();
		
		// to store average precision of every run
		List<Float> avgPrecisionBaseline_list = new ArrayList<Float>();
		List<Float> avgPrecisionManualExpansion_list = new ArrayList<Float>();
		List<Float> avgPrecision_a1_b1_list = new ArrayList<Float>();
		List<Float> avgPrecision_a1_b2_1_list = new ArrayList<Float>();
		List<Float> avgPrecision_a1_b2_2_list = new ArrayList<Float>();
		List<Float> avgPrecision_a3_b1_list = new ArrayList<Float>();
		List<Float> avgPrecision_a3_b2_1_list = new ArrayList<Float>();
		List<Float> avgPrecision_a3_b2_2_list = new ArrayList<Float>();
		
		// to store precision recall values of every run
		List<PrecisionRecallStorage> prBaseline_list = new ArrayList<PrecisionRecallStorage>();
		List<PrecisionRecallStorage> prManualExpansion_list = new ArrayList<PrecisionRecallStorage>();
		List<PrecisionRecallStorage> pr_a1_b1_list = new ArrayList<PrecisionRecallStorage>();
		List<PrecisionRecallStorage> pr_a1_b2_1_list = new ArrayList<PrecisionRecallStorage>();
		List<PrecisionRecallStorage> pr_a1_b2_2_list = new ArrayList<PrecisionRecallStorage>();
		List<PrecisionRecallStorage> pr_a3_b1_list = new ArrayList<PrecisionRecallStorage>();
		List<PrecisionRecallStorage> pr_a3_b2_1_list = new ArrayList<PrecisionRecallStorage>();
		List<PrecisionRecallStorage> pr_a3_b2_2_list = new ArrayList<PrecisionRecallStorage>();
		
		Set<Integer> docsForEvaluation = new HashSet<Integer>();
		Set<Integer> docsForRelevanceFeedback = new HashSet<Integer>();
		int goldstandardSize = DataUtils.checkGoldstandard(goldstandard);
		int intervalSize = goldstandardSize / GOLDSTANDARD_SPLIT;
		for (int i = 0; i < GOLDSTANDARD_SPLIT; i++) {
			initializeGoldSplit(docsForEvaluation, docsForRelevanceFeedback, goldstandardSize, intervalSize, i);
//			System.out.println("EVAL-SET: " + docsForEvaluation);
//			System.out.println("RF-SET:   " + docsForRelevanceFeedback);
			
			// don't show output
			if (!showVerboseIndexing) {
//				System.out.println("Building index...");
				System.setOut(new NullPrintStream());
				System.setErr(new NullPrintStream());
			}
			
			System.out.println("-----------------------");
			System.out.println(" BUILDING INDEX");
			System.out.println("-----------------------");
			
			// build default index
			createFsIndexFromCsv(indexPathBase, processedCleanedDataInfile, goldstandard, docsForEvaluation);
			// build index with boosted documents
			List<AssessableDocument> docsToAssess = DataUtils.buildDocsToAssess(goldstandard, docsForEvaluation, processedCleanedDataInfile);
			// separate goldstandard into relevance feedback (= training) and documents to assess (= evaluation)
			Map<String, Double> boostsFromHighlights = topicModel.getBoostFactorsFromHighlights(DataUtils.buildRelevanceFeedbackWithHighlights(goldstandard, docsForRelevanceFeedback), docsToAssess);
			createFsIndexFromCsv(indexPathBoostHighlights, processedCleanedDataInfile, goldstandard, docsForEvaluation, boostsFromHighlights);
			// separate goldstandard into relevance feedback (= training) and documents to assess (= evaluation)
			Map<String, Double> boostsFromEntireContent = topicModel.getBoostFactorsFromEntireContents(DataUtils.buildRelevanceFeedbackWithContents(goldstandard, docsForRelevanceFeedback, processedCleanedDataInfile), docsToAssess);
			createFsIndexFromCsv(indexPathBoostEntireContent, processedCleanedDataInfile, goldstandard, docsForEvaluation, boostsFromEntireContent);
			
			// show output again
			System.setOut(sysout);
			System.setErr(syserr);
			
			// don't show output
			if (!showVerboseExperiments) {
				System.out.println("Running experiments... (" + (i+1) + "/" + GOLDSTANDARD_SPLIT + ")");
				System.setOut(new NullPrintStream());
				System.setErr(new NullPrintStream());
			}
			
			System.out.println("-----------------------");
			System.out.println(" RUNNING EXPERIMENTS (" + (i+1) + "/" + GOLDSTANDARD_SPLIT + ")");
			System.out.println("-----------------------");
			
			// expand query with words from dominant topic of all relevant documents
			List<ExpandableTerm> expandableTermsFromEntireContent = topicModel.getSimilarTermsFromEntireContents(DataUtils.buildRelevanceFeedbackWithContents(goldstandard, docsForRelevanceFeedback, processedCleanedDataInfile), NUM_TERMS);
			String expandedQueryByEntireContent = baseSearchString;
			for (ExpandableTerm expandableTerm : expandableTermsFromEntireContent) {
				expandedQueryByEntireContent += " " + expandableTerm.getTerm();
			}
			
			// expand query with words from dominant topic of all highlights of relevant documents
			List<ExpandableTerm> expandableTermsFromHighlights = topicModel.getSimilarTermsFromHighlights(DataUtils.buildRelevanceFeedbackWithHighlights(goldstandard, docsForRelevanceFeedback), NUM_TERMS);
			String expandedQueryByHighlights = baseSearchString;
			for (ExpandableTerm expandableTerm : expandableTermsFromHighlights) {
				expandedQueryByHighlights += " " + expandableTerm.getTerm();
			}
			
			// search in index
			System.out.println("~ baseline ~");
			ResultDocument[] searchResults_baseline = searchFsIndex(indexPathBase, baseSearchString, NUMBER_OF_SEARCH_RESULTS, null);
			System.out.println("~ manual query expansion ~");
			ResultDocument[] searchResults_manualExpansion = searchFsIndex(indexPathBase, expandedSearchString, NUMBER_OF_SEARCH_RESULTS, null);
			System.out.println("~ a1-b1 (document-documentboost) ~");
			ResultDocument[] searchResults_a1_b1 = searchFsIndex(indexPathBoostEntireContent, baseSearchString, NUMBER_OF_SEARCH_RESULTS, null);
			System.out.println("~ a1-b2.1 (document-queryexpansion) ~");
			ResultDocument[] searchResults_a1_b2_1 = searchFsIndex(indexPathBase, expandedQueryByEntireContent, NUMBER_OF_SEARCH_RESULTS, null);
			System.out.println("~ a1-b2.2 (document-customscore) ~");
			ResultDocument[] searchResults_a1_b2_2 = searchFsIndex(indexPathBase, baseSearchString, NUMBER_OF_SEARCH_RESULTS, expandableTermsFromEntireContent);
			System.out.println("~ a3-b1 (highlights-documentboost) ~");
			ResultDocument[] searchResults_a3_b1 = searchFsIndex(indexPathBoostHighlights, baseSearchString, NUMBER_OF_SEARCH_RESULTS, null);
			System.out.println("~ a3-b2.1 (highlights-queryexpansion) ~");
			ResultDocument[] searchResults_a3_b2_1 = searchFsIndex(indexPathBase, expandedQueryByHighlights, NUMBER_OF_SEARCH_RESULTS, null);
			System.out.println("~ a3-b2.2 (highlights-customscore) ~");
			ResultDocument[] searchResults_a3_b2_2 = searchFsIndex(indexPathBase, baseSearchString, NUMBER_OF_SEARCH_RESULTS, expandableTermsFromHighlights);
			
			// show output again
			System.setOut(sysout);
			System.setErr(syserr);
			
			// don't show output
			if (!showVerboseResults) {
				System.setOut(new NullPrintStream());
				System.setErr(new NullPrintStream());
			}
			
			System.out.println("-----------------------");
			System.out.println(" RESULTS: " + queryAspectID + " (" + (i+1) + "/" + GOLDSTANDARD_SPLIT + ")");
			System.out.println("-----------------------");
			
			// show precision at 10
			System.out.println("[Precision at " + PAK + "]");
			float PAK_baseline = calculatePrecisionAtK(searchResults_baseline, goldstandard, docsForEvaluation, PAK);
			float PAK_manualExpansion = calculatePrecisionAtK(searchResults_manualExpansion, goldstandard, docsForEvaluation, PAK);
			float PAK_a1_b1 = calculatePrecisionAtK(searchResults_a1_b1, goldstandard, docsForEvaluation, PAK);
			float PAK_a1_b2_1 = calculatePrecisionAtK(searchResults_a1_b2_1, goldstandard, docsForEvaluation, PAK);
			float PAK_a1_b2_2 = calculatePrecisionAtK(searchResults_a1_b2_2, goldstandard, docsForEvaluation, PAK);
			float PAK_a3_b1 = calculatePrecisionAtK(searchResults_a3_b1, goldstandard, docsForEvaluation, PAK);
			float PAK_a3_b2_1 = calculatePrecisionAtK(searchResults_a3_b2_1, goldstandard, docsForEvaluation, PAK);
			float PAK_a3_b2_2 = calculatePrecisionAtK(searchResults_a3_b2_2, goldstandard, docsForEvaluation, PAK);
			System.out.printf("Baseline:         %.0f%%   (+ %.0f%%)%n", PAK_baseline*100, (PAK_baseline-PAK_baseline)*100);
			System.out.printf("manual expansion: %.0f%%   (+ %.0f%%)%n", PAK_manualExpansion*100, (PAK_manualExpansion-PAK_baseline)*100);
			System.out.printf("a1-b1:            %.0f%%   (+ %.0f%%)%n", PAK_a1_b1*100, (PAK_a1_b1-PAK_baseline)*100);
			System.out.printf("a1-b2.1:          %.0f%%   (+ %.0f%%)%n", PAK_a1_b2_1*100, (PAK_a1_b2_1-PAK_baseline)*100);
			System.out.printf("a1-b2.2:          %.0f%%   (+ %.0f%%)%n", PAK_a1_b2_2*100, (PAK_a1_b2_2-PAK_baseline)*100);
			System.out.printf("a3-b1:            %.0f%%   (+ %.0f%%)%n", PAK_a3_b1*100, (PAK_a3_b1-PAK_baseline)*100);
			System.out.printf("a3-b2.1:          %.0f%%   (+ %.0f%%)%n", PAK_a3_b2_1*100, (PAK_a3_b2_1-PAK_baseline)*100);
			System.out.printf("a3-b2.2:          %.0f%%   (+ %.0f%%)%n", PAK_a3_b2_2*100, (PAK_a3_b2_2-PAK_baseline)*100);
			PAK_baseline_list.add(PAK_baseline);
			PAK_manualExpansion_list.add(PAK_manualExpansion);
			PAK_a1_b1_list.add(PAK_a1_b1);
			PAK_a1_b2_1_list.add(PAK_a1_b2_1);
			PAK_a1_b2_2_list.add(PAK_a1_b2_2);
			PAK_a3_b1_list.add(PAK_a3_b1);
			PAK_a3_b2_1_list.add(PAK_a3_b2_1);
			PAK_a3_b2_2_list.add(PAK_a3_b2_2);
			
			// show average precision
			System.out.println("[Average Precision]");
			float avgPrecisionBaseline = calculateAveragePrecision(searchResults_baseline, goldstandard, docsForEvaluation);
			float avgPrecisionManualExpansion = calculateAveragePrecision(searchResults_manualExpansion, goldstandard, docsForEvaluation);
			float avgPrecision_a1_b1 = calculateAveragePrecision(searchResults_a1_b1, goldstandard, docsForEvaluation);
			float avgPrecision_a1_b2_1 = calculateAveragePrecision(searchResults_a1_b2_1, goldstandard, docsForEvaluation);
			float avgPrecision_a1_b2_2 = calculateAveragePrecision(searchResults_a1_b2_2, goldstandard, docsForEvaluation);
			float avgPrecision_a3_b1 = calculateAveragePrecision(searchResults_a3_b1, goldstandard, docsForEvaluation);
			float avgPrecision_a3_b2_1 = calculateAveragePrecision(searchResults_a3_b2_1, goldstandard, docsForEvaluation);
			float avgPrecision_a3_b2_2 = calculateAveragePrecision(searchResults_a3_b2_2, goldstandard, docsForEvaluation);
			System.out.printf("Baseline:         %.2f%%   (+ %.2f%%)%n", avgPrecisionBaseline*100, (avgPrecisionBaseline-avgPrecisionBaseline)*100);
			System.out.printf("manual expansion: %.2f%%   (+ %.2f%%)%n", avgPrecisionManualExpansion*100, (avgPrecisionManualExpansion-avgPrecisionBaseline)*100);
			System.out.printf("a1-b1:            %.2f%%   (+ %.2f%%)%n", avgPrecision_a1_b1*100, (avgPrecision_a1_b1-avgPrecisionBaseline)*100);
			System.out.printf("a1-b2.1:          %.2f%%   (+ %.2f%%)%n", avgPrecision_a1_b2_1*100, (avgPrecision_a1_b2_1-avgPrecisionBaseline)*100);
			System.out.printf("a1-b2.2:          %.2f%%   (+ %.2f%%)%n", avgPrecision_a1_b2_2*100, (avgPrecision_a1_b2_2-avgPrecisionBaseline)*100);
			System.out.printf("a3-b1:            %.2f%%   (+ %.2f%%)%n", avgPrecision_a3_b1*100, (avgPrecision_a3_b1-avgPrecisionBaseline)*100);
			System.out.printf("a3-b2.1:          %.2f%%   (+ %.2f%%)%n", avgPrecision_a3_b2_1*100, (avgPrecision_a3_b2_1-avgPrecisionBaseline)*100);
			System.out.printf("a3-b2.2:          %.2f%%   (+ %.2f%%)%n", avgPrecision_a3_b2_2*100, (avgPrecision_a3_b2_2-avgPrecisionBaseline)*100);
			avgPrecisionBaseline_list.add(avgPrecisionBaseline);
			avgPrecisionManualExpansion_list.add(avgPrecisionManualExpansion);
			avgPrecision_a1_b1_list.add(avgPrecision_a1_b1);
			avgPrecision_a1_b2_1_list.add(avgPrecision_a1_b2_1);
			avgPrecision_a1_b2_2_list.add(avgPrecision_a1_b2_2);
			avgPrecision_a3_b1_list.add(avgPrecision_a3_b1);
			avgPrecision_a3_b2_1_list.add(avgPrecision_a3_b2_1);
			avgPrecision_a3_b2_2_list.add(avgPrecision_a3_b2_2);
			
			// calculate precision/recall values
			// show output to see warnings when creating the precision recall data structure
//			System.setOut(sysout);
//			System.setErr(syserr);
			prBaseline_list.add(getPrecisionRecallValues(searchResults_baseline, goldstandard, docsForEvaluation));
			prManualExpansion_list.add(getPrecisionRecallValues(searchResults_manualExpansion, goldstandard, docsForEvaluation));
			pr_a1_b1_list.add(getPrecisionRecallValues(searchResults_a1_b1, goldstandard, docsForEvaluation));
			pr_a1_b2_1_list.add(getPrecisionRecallValues(searchResults_a1_b2_1, goldstandard, docsForEvaluation));
			pr_a1_b2_2_list.add(getPrecisionRecallValues(searchResults_a1_b2_2, goldstandard, docsForEvaluation));
			pr_a3_b1_list.add(getPrecisionRecallValues(searchResults_a3_b1, goldstandard, docsForEvaluation));
			pr_a3_b2_1_list.add(getPrecisionRecallValues(searchResults_a3_b2_1, goldstandard, docsForEvaluation));
			pr_a3_b2_2_list.add(getPrecisionRecallValues(searchResults_a3_b2_2, goldstandard, docsForEvaluation));
			
			// show output again
			System.setOut(sysout);
			System.setErr(syserr);
			
		}
		
		System.out.println("---------------------------------------------------");
		System.out.println(" AVERAGED RESULTS OVER ALL RUNS: " + queryAspectID);
		System.out.println("---------------------------------------------------");
		
		System.out.println("[Average Precision at " + PAK + "]");
		float avgPrecAtK_baseline = calculateAverage(PAK_baseline_list);
		float avgPrecAtK_manualExpansion = calculateAverage(PAK_manualExpansion_list);
		float avgPrecAtK_a1_b1 = calculateAverage(PAK_a1_b1_list);
		float avgPrecAtK_a1_b2_1 = calculateAverage(PAK_a1_b2_1_list);
		float avgPrecAtK_a1_b2_2 = calculateAverage(PAK_a1_b2_2_list);
		float avgPrecAtK_a3_b1 = calculateAverage(PAK_a3_b1_list);
		float avgPrecAtK_a3_b2_1 = calculateAverage(PAK_a3_b2_1_list);
		float avgPrecAtK_a3_b2_2 = calculateAverage(PAK_a3_b2_2_list);
		float avgPAKstdDev_baseline = calculateStandardDeviation(PAK_baseline_list);
		float avgPAKstdDev_manualExpansion = calculateStandardDeviation(PAK_manualExpansion_list);
		float avgPAKstdDev_a1_b1 = calculateStandardDeviation(PAK_a1_b1_list);
		float avgPAKstdDev_a1_b2_1 = calculateStandardDeviation(PAK_a1_b2_1_list);
		float avgPAKstdDev_a1_b2_2 = calculateStandardDeviation(PAK_a1_b2_2_list);
		float avgPAKstdDev_a3_b1 = calculateStandardDeviation(PAK_a3_b1_list);
		float avgPAKstdDev_a3_b2_1 = calculateStandardDeviation(PAK_a3_b2_1_list);
		float avgPAKstdDev_a3_b2_2 = calculateStandardDeviation(PAK_a3_b2_2_list);
		System.out.printf("baseline                %.0f%% (s=%.0f%%) \t +%.0f%% %n", avgPrecAtK_baseline*100, avgPAKstdDev_baseline*100, (avgPrecAtK_baseline-avgPrecAtK_baseline)*100);
		System.out.printf("manual query expansion  %.0f%% (s=%.0f%%) \t +%.0f%% %n", avgPrecAtK_manualExpansion*100, avgPAKstdDev_manualExpansion*100, (avgPrecAtK_manualExpansion-avgPrecAtK_baseline)*100);
		System.out.printf("a1-b1                   %.0f%% (s=%.0f%%) \t +%.0f%% %n", avgPrecAtK_a1_b1*100, avgPAKstdDev_a1_b1*100, (avgPrecAtK_a1_b1-avgPrecAtK_baseline)*100);
		System.out.printf("a1-b2.1                 %.0f%% (s=%.0f%%) \t +%.0f%% %n", avgPrecAtK_a1_b2_1*100, avgPAKstdDev_a1_b2_1*100, (avgPrecAtK_a1_b2_1-avgPrecAtK_baseline)*100);
		System.out.printf("a1-b2.2                 %.0f%% (s=%.0f%%) \t +%.0f%% %n", avgPrecAtK_a1_b2_2*100, avgPAKstdDev_a1_b2_2*100, (avgPrecAtK_a1_b2_2-avgPrecAtK_baseline)*100);
		System.out.printf("a3-b1                   %.0f%% (s=%.0f%%) \t +%.0f%% %n", avgPrecAtK_a3_b1*100, avgPAKstdDev_a3_b1*100, (avgPrecAtK_a3_b1-avgPrecAtK_baseline)*100);
		System.out.printf("a3-b2.1                 %.0f%% (s=%.0f%%) \t +%.0f%% %n", avgPrecAtK_a3_b2_1*100, avgPAKstdDev_a3_b2_1*100, (avgPrecAtK_a3_b2_1-avgPrecAtK_baseline)*100);
		System.out.printf("a3-b2.2                 %.0f%% (s=%.0f%%) \t +%.0f%% %n", avgPrecAtK_a3_b2_2*100, avgPAKstdDev_a3_b2_2*100, (avgPrecAtK_a3_b2_2-avgPrecAtK_baseline)*100);
		
		System.out.println("[Mean Average Precision]");
		float MAPBaseline = calculateAverage(avgPrecisionBaseline_list);
		float MAPManualExpansion = calculateAverage(avgPrecisionManualExpansion_list);
		float MAP_a1_b1 = calculateAverage(avgPrecision_a1_b1_list);
		float MAP_a1_b2_1 = calculateAverage(avgPrecision_a1_b2_1_list);
		float MAP_a1_b2_2 = calculateAverage(avgPrecision_a1_b2_2_list);
		float MAP_a3_b1 = calculateAverage(avgPrecision_a3_b1_list);
		float MAP_a3_b2_1 = calculateAverage(avgPrecision_a3_b2_1_list);
		float MAP_a3_b2_2 = calculateAverage(avgPrecision_a3_b2_2_list);
		float MAPstdDevBaseline = calculateStandardDeviation(avgPrecisionBaseline_list);
		float MAPstdDevManualExpansion = calculateStandardDeviation(avgPrecisionManualExpansion_list);
		float MAPstdDev_a1_b1 = calculateStandardDeviation(avgPrecision_a1_b1_list);
		float MAPstdDev_a1_b2_1 = calculateStandardDeviation(avgPrecision_a1_b2_1_list);
		float MAPstdDev_a1_b2_2 = calculateStandardDeviation(avgPrecision_a1_b2_2_list);
		float MAPstdDev_a3_b1 = calculateStandardDeviation(avgPrecision_a3_b1_list);
		float MAPstdDev_a3_b2_1 = calculateStandardDeviation(avgPrecision_a3_b2_1_list);
		float MAPstdDev_a3_b2_2 = calculateStandardDeviation(avgPrecision_a3_b2_2_list);
		System.out.printf("baseline                %.0f%% (s=%.0f%%) \t +%.0f%% %n", MAPBaseline*100, MAPstdDevBaseline*100, (MAPBaseline-MAPBaseline)*100);
		System.out.printf("manual query expansion  %.0f%% (s=%.0f%%) \t +%.0f%% %n", MAPManualExpansion*100, MAPstdDevManualExpansion*100, (MAPManualExpansion-MAPBaseline)*100);
		System.out.printf("a1-b1                   %.0f%% (s=%.0f%%) \t +%.0f%% %n", MAP_a1_b1*100, MAPstdDev_a1_b1*100, (MAP_a1_b1-MAPBaseline)*100);
		System.out.printf("a1-b2.1                 %.0f%% (s=%.0f%%) \t +%.0f%% %n", MAP_a1_b2_1*100, MAPstdDev_a1_b2_1*100, (MAP_a1_b2_1-MAPBaseline)*100);
		System.out.printf("a1-b2.2                 %.0f%% (s=%.0f%%) \t +%.0f%% %n", MAP_a1_b2_2*100, MAPstdDev_a1_b2_2*100, (MAP_a1_b2_2-MAPBaseline)*100);
		System.out.printf("a3-b1                   %.0f%% (s=%.0f%%) \t +%.0f%% %n", MAP_a3_b1*100, MAPstdDev_a3_b1*100, (MAP_a3_b1-MAPBaseline)*100);
		System.out.printf("a3-b2.1                 %.0f%% (s=%.0f%%) \t +%.0f%% %n", MAP_a3_b2_1*100, MAPstdDev_a3_b2_1*100, (MAP_a3_b2_1-MAPBaseline)*100);
		System.out.printf("a3-b2.2                 %.0f%% (s=%.0f%%) \t +%.0f%% %n", MAP_a3_b2_2*100, MAPstdDev_a3_b2_2*100, (MAP_a3_b2_2-MAPBaseline)*100);
		
		// don't show output
		if (!showVerboseStoring) {
			System.out.println("Storing precision recall values in files...");
			System.setOut(new NullPrintStream());
			System.setErr(new NullPrintStream());
		}
		
		writePrecisionRecallAvgsToFile(prBaseline_list, resultFileBaseline);
		writePrecisionRecallAvgsToFile(prManualExpansion_list, resultFileManualExpansion);
		writePrecisionRecallAvgsToFile(pr_a1_b1_list, resultFile_a1_b1);
		writePrecisionRecallAvgsToFile(pr_a1_b2_1_list, resultFile_a1_b2_1);
		writePrecisionRecallAvgsToFile(pr_a1_b2_2_list, resultFile_a1_b2_2);
		writePrecisionRecallAvgsToFile(pr_a3_b1_list, resultFile_a3_b1);
		writePrecisionRecallAvgsToFile(pr_a3_b2_1_list, resultFile_a3_b2_1);
		writePrecisionRecallAvgsToFile(pr_a3_b2_2_list, resultFile_a3_b2_2);
		
		// show output again
		System.setOut(sysout);
		System.setErr(syserr);
		
		System.out.println("Precion/Recall values stored in\n   "
				+ resultFileBaseline + "\n   "
				+ resultFileManualExpansion + "\n   "
				+ resultFile_a1_b1 + "\n   "
				+ resultFile_a1_b2_1 + "\n   "
				+ resultFile_a1_b2_2 + "\n   "
				+ resultFile_a3_b1 + "\n   "
				+ resultFile_a3_b2_1 + "\n   "
				+ resultFile_a3_b2_2);
	}

	private static float calculateAverage(List<Float> list) {
		float sum = 0f;
		for (Float val : list) {
			sum += val;
		}
		return sum / list.size();
	}
	
	// https://de.wikipedia.org/wiki/Stichprobenvarianz
	private static float calculateStandardDeviation(List<Float> list) {
		float x_avg = calculateAverage(list);
		int n = list.size();
		float sum = 0;
		Double resultDouble;
		for (Float x_i : list) {
			sum += Math.pow((x_i - x_avg), 2d);
		}
		resultDouble = Math.sqrt(sum / (n - 1));
		return resultDouble.floatValue();
	}

	private static void initializeGoldSplit(Set<Integer> docsForEvaluation, Set<Integer> docsForRelevanceFeedback,
			int goldstandardSize, int intervalSize, int i) {
		docsForEvaluation.clear();
		docsForRelevanceFeedback.clear();
		
		for (int k = 1; k <= goldstandardSize; k++) {
			docsForEvaluation.add(k);
		}
		
		int rfDocsStart = i * intervalSize + 1;
		int rfDocsEnd = (i * intervalSize + intervalSize);
		for (int j = rfDocsStart; j <= rfDocsEnd; j++) {
			docsForRelevanceFeedback.add(j);
			docsForEvaluation.remove(j);
		}
	}
	
	private static PrecisionRecallStorage getPrecisionRecallValues(ResultDocument[] searchResults, String goldstandard, Set<Integer> goldDocs) throws InvalidRelevanceException, IOException {
		PrecisionRecallStorage prVals = new PrecisionRecallStorage();
		Map<String, Boolean> relevanceMap = DataUtils.getRelevanceJudgements(goldstandard, goldDocs);
		int totalRelevant = DataUtils.getTotalRelevant(goldstandard, goldDocs);
		int numRelevant = 0;
		
		for (int k = 0; k < searchResults.length; k++) {
			Boolean judgment = relevanceMap.get(searchResults[k].getDocID());
			if (judgment == null) {
				System.err.println("WARNING: No judgment found for document '" + searchResults[k].getDocID() + "'!");
			}
			// if search result is relevant
			else if (judgment) {
				numRelevant++;
			}
			float precision = (float)numRelevant/(k+1);
			float recall = (float)numRelevant/totalRelevant;
			prVals.add(k, recall, precision);
		}
		return prVals;
	}
	
	private static void writePrecisionRecallAvgsToFile(List<PrecisionRecallStorage> values, String outfile) throws IOException, NoMatchingRecallValueException {
		
		PrecisionRecallStorage averages = combinePrecisionRecallStorages(values);
//		System.out.println("COMBINED AVGS: " + averages);
		
		File outDir = new File(outfile).getParentFile();
		if (!outDir.exists()) {
			if (outDir.mkdirs()) {
				System.out.println("Directory '" + outDir + "' created.");
			} else {
				System.out.println("Directory '" + outDir + "' could not be created!");
			}
		}
		FileWriter fw = new FileWriter(outfile);
		fw.write("k\trecall\tP@k\n");
		fw.write("-1\t0\t0\tNO REAL DATA!\n"); // always start at recall/precision (0,0) for easier plotting
		
		for (Entry<Integer, PrecisionRecallPair<Float, Float>> entry : averages) {
			int k = entry.getKey();
			float recall = entry.getValue().getRecall();
			float precision = entry.getValue().getPrecision();
			fw.write(k + "\t" + recall + "\t" + precision + "\n");
		}
		fw.write("-1\t2\t0\tNO REAL DATA!\n"); // always end at recall/precision (2,0) for easier plotting
		fw.close();
	}

	private static PrecisionRecallStorage combinePrecisionRecallStorages(List<PrecisionRecallStorage> values) throws NoMatchingRecallValueException {
		PrecisionRecallStorage averagedStorage = new PrecisionRecallStorage();
		
		// <k, recall values at k from all runs>
		Map<Integer, List<Float>> recallVals = new HashMap<>();
		// <k, precision values at k all runs>
		Map<Integer, List<Float>> precisionVals = new HashMap<>();
		
		// gather all precision and recall values from all runs
		for (PrecisionRecallStorage prRunEntity : values) {
			for (Entry<Integer, PrecisionRecallPair<Float, Float>> entry : prRunEntity) {
				int k = entry.getKey();
				PrecisionRecallPair<Float, Float> pair = entry.getValue();
				// add recall value to list in bucket k
				if (recallVals.get(k) == null) {
					recallVals.put(k, new ArrayList<Float>());
				}
				recallVals.get(k).add(pair.getRecall());
				// add precision value to list in bucket k
				if (precisionVals.get(k) == null) {
					precisionVals.put(k, new ArrayList<Float>());
				}
				precisionVals.get(k).add(pair.getPrecision());
			}
		}
		
		// calculate averages of all recall values
		for (Entry<Integer, List<Float>> kthEntry : recallVals.entrySet()) {
			int k = kthEntry.getKey();
			List<Float> recallList = kthEntry.getValue();
			float avgRecallAtK = calculateAverage(recallList);
			averagedStorage.add(k, avgRecallAtK, -1f); // fill in average precision later 
		}
		// calculate averages of all precision values
		for (Entry<Integer, List<Float>> kthEntry : precisionVals.entrySet()) {
			int k = kthEntry.getKey();
			List<Float> precisionList = kthEntry.getValue();
			float avgPrecisionAtK = calculateAverage(precisionList);
			if (averagedStorage.at(k) == null) {
				throw new NoMatchingRecallValueException("No PrecisionRecallPair found at " + k + ". Something must have gone wrong when calculating the average recall values.");
			}
			averagedStorage.add(k, averagedStorage.at(k).getRecall(), avgPrecisionAtK);
		}
		
		return averagedStorage;
	}

	private static float calculateAveragePrecision(ResultDocument[] searchResults, String goldstandard, Set<Integer> goldDocs) throws InvalidRelevanceException, IOException {
		Map<String, Boolean> relevanceMap = DataUtils.getRelevanceJudgements(goldstandard, goldDocs);
		int numRelevant = 0;
		float precisionSum = 0;
		for (int k = 0; k < searchResults.length; k++) {
			Boolean judgment = relevanceMap.get(searchResults[k].getDocID());
			if (judgment == null) {
				System.err.println("WARNING: No judgment found for document '" + searchResults[k].getDocID()
						+ "'! Make sure your evaluation set contains a judgment for " + searchResults[k].getDocID() + ".");
			}
			// if search result is relevant
			else if (judgment) {
				numRelevant++;
				float precisionAtK = (float)numRelevant/(k+1);
				precisionSum += precisionAtK;
			}
		}
		return precisionSum/numRelevant;
	}

	private static float calculatePrecisionAtK(ResultDocument[] searchResults, String goldstandard, Set<Integer> goldDocs, int k) throws InvalidRelevanceException, IOException {
		Map<String, Boolean> relevanceMap = DataUtils.getRelevanceJudgements(goldstandard, goldDocs);
		int numRelevant = 0;
		int numResults = searchResults.length;
		if (k > numResults) {
			k = numResults;
			System.out.println("Only " + numResults + " search results available. k changed to " + k + ".");
		}
		for (int i = 0; i < k; i++) {
			Boolean judgment = relevanceMap.get(searchResults[i].getDocID());
			if (judgment == null) {
				System.err.println("WARNING: No judgment found for document '" + searchResults[i].getDocID()
						+ "'! Make sure your evaluation set contains a judgment for " + searchResults[i].getDocID() + ".");
			}
			// if search result is relevant
			else if (judgment) {
				numRelevant++;
			}
		}
		if (k <= 0) {
			System.err.println("k must be greater than zero for Precision at k!");
			return -1;
		} else {
			float precision = (float)numRelevant/k;
			return precision;
		}
	}

	/**
	 * Creates an index from documents which are both in {@code csv} and {@code goldstandard}.
	 * 
	 * @param indexDir
	 * @param csv
	 * @param goldstandard
	 * @param docsToIndex Elements to index in goldstandard.
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	private static void createFsIndexFromCsv(String indexDir, String csv, String goldstandard, Set<Integer> docsToIndex) throws IOException, FileNotFoundException {
		File csvFile = new File(csv);
		File indexDirFile = new File(indexDir);
		File goldstandardFile = new File(goldstandard);
		long start = new Date().getTime();
		Indexer.buildFSIndexFromCsv(indexDirFile, csvFile, goldstandardFile, docsToIndex);
		long end = new Date().getTime();
		System.err.println("Index created under " + indexDir + " (in " + (end - start) + " milliseconds)");
	}
	
	/**
	 * Creates index which boosts entire documents according to {@code boostFactors}.
	 * 
	 * @param indexDir
	 * @param csv
	 * @param goldstandard
	 * @param docsToIndex Element to index in goldstandard.
	 * @param boostFactors
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	private static void createFsIndexFromCsv(String indexDir, String csv, String goldstandard, Set<Integer> docsToIndex, Map<String, Double> boostFactors) throws IOException, FileNotFoundException {
		File csvFile = new File(csv);
		File indexDirFile = new File(indexDir);
		File goldstandardFile = new File(goldstandard);
		long start = new Date().getTime();
		Indexer.buildFSIndexFromCsv(indexDirFile, csvFile, goldstandardFile, docsToIndex, boostFactors);
		long end = new Date().getTime();
		System.err.println("Index created under " + indexDir + " (in " + (end - start) + " milliseconds)");
	}

	/**
	 * Search in filesystem index.
	 * 
	 * @param indexPath Index to search.
	 * @param query
	 * @param numberOfResults
	 * @param boosts If not {@code null}, search will use custom score function to boost documents in {@code boosts}.
	 * If {@code null} default scoring (tf-idf) will be used.
	 * @return
	 * @throws Exception
	 */
	private static ResultDocument[] searchFsIndex(String indexPath, String query, int numberOfResults, List<ExpandableTerm> boosts) throws Exception {
		System.err.println("Searching in disk index " + indexPath + "...");
		File fsIndexFile = new File(indexPath);
		if (!fsIndexFile.exists() || !fsIndexFile.isDirectory()) {
			throw new IOException(fsIndexFile + " does not exist or is not a directory.");
		}
		Directory fsIndexDir = FSDirectory.open(fsIndexFile.toPath());
		return Searcher.search(fsIndexDir, query, numberOfResults, false, false, boosts);
	}

}
