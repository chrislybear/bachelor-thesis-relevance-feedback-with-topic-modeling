package ch.uzh.lucene;

import static ch.uzh.main.Config.*;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.classic.QueryParserBase;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.MultiTermQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;

import ch.uzh.data.ExpandableTerm;
import ch.uzh.data.ResultDocument;

public class Searcher {
	
	/**
	 * Search for a query in index. Use default values for fields (as defined in Config.java).
	 * 
	 * @param indexDir Directory where index is stored.
	 * @param queryString Query terms separated by whitespaces. To boost a term write {@code term^2.0}.
	 * @param numResults Number of search results to display.
	 * @param showResults If {@code true} prints list of documents that matched query.
	 * @param showExplanation If {@code true} prints explanation of score for each document.
	 * @param boosts If not {@code null}, search will use custom score function to boost documents in {@code boosts}.
	 * If {@code null} default scoring (tf-idf) will be used.
	 * @return
	 * @throws Exception
	 */
	public static ResultDocument[] search(Directory indexDir, String queryString, int numResults, boolean showResults, boolean showExplanation, List<ExpandableTerm> boosts) throws Exception {
		return search(indexDir, queryString, numResults, showResults, showExplanation, CONTENT_FIELD, DOCID_FIELD, boosts);
	}

	/**
	 * Search for a query in index.
	 * 
	 * @param indexDir Directory where index is stored.
	 * @param queryString Query terms separated by whitespaces. To boost a term write {@code term^2.0}.
	 * @param fieldToSearch Field to search in.
	 * @param docIdField Field that identifies the document. Will be shown in search results.
	 * @param numResults Number of search results to display.
	 * @param showResults If {@code true} prints list of documents that matched query.
	 * @param showExplanation If {@code true} prints explanation of score for each document.
	 * @param boosts If not {@code null}, search will use custom score function to boost documents in {@code boosts}.
	 * If {@code null} default scoring (tf-idf) will be used.
	 * @throws Exception
	 */
	public static ResultDocument[] search(Directory indexDir, String queryString, int numResults, boolean showResults, boolean showExplanation, String fieldToSearch, String docIdField, List<ExpandableTerm> boosts)
			throws Exception {

		DirectoryReader reader = DirectoryReader.open(indexDir);
		IndexSearcher searcher = new IndexSearcher(reader);
		
		Query query = buildQuery(queryString, fieldToSearch, boosts);
		
		long start = new Date().getTime();
		TopDocs hits = searcher.search(query, numResults);
		long end = new Date().getTime();
		// Search index
		System.err.println("Found " + hits.totalHits + " document(s) (in "
		+ (end - start) + " milliseconds) that matched query '" + queryString + "'");
		DecimalFormat scoreFormat = new DecimalFormat("0.00");
		
		// actual number of results is minimum of wished number of results and number of search hits
		int numActualResults = Math.min(numResults, hits.totalHits);
		ResultDocument[] results = new ResultDocument[numActualResults];
		
		for (int i = 0; i < numActualResults; i++) {
			ScoreDoc scoreDoc = hits.scoreDocs[i];
			int indexDocId = scoreDoc.doc;
			Document doc = searcher.doc(indexDocId);
			// add document to results
			results[i] = new ResultDocument(doc.get(docIdField), scoreDoc.score);
			if (showResults) {
				// print score and document identifier
				System.out.println(scoreFormat.format(scoreDoc.score) + "\t" + doc.get(docIdField));
			}
			if (showExplanation) {
				// print explanation how query-document score was calculated
				System.err.println(searcher.explain(query, indexDocId).toString());	
			}
		}
		return results;
	}

	private static Query buildQuery(String queryString, String fieldToSearch, List<ExpandableTerm> boosts) throws IOException {
		Reader stopwordsDe = new FileReader(STOPWORDS_DE_FILE);
		QueryParser qp = new QueryParser(fieldToSearch, new StandardAnalyzer(stopwordsDe));
		// rewrites prefix query as a boolean query and keeps the document boosts
		// (this is needed to account for the boosts at indexing time (see Indexer.java)
		qp.setMultiTermRewriteMethod(MultiTermQuery.SCORING_BOOLEAN_REWRITE);
		String escapedQueryString = QueryParserBase.escape(queryString);
		Query q = null;
		try {
			q = qp.parse(escapedQueryString);
			if (boosts != null) {
				q = new MyScoreQuery(q, boosts);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		System.out.println("Query parsed as " + q.getClass().getSimpleName() + ": " + q.toString());
		
		return q;
	}

}
