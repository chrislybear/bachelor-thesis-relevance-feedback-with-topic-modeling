package ch.uzh.lucene;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.lucene.index.LeafReader;
import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.queries.CustomScoreProvider;
import org.apache.lucene.queries.CustomScoreQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.util.BytesRef;

import ch.uzh.data.ExpandableTerm;

import static ch.uzh.main.Config.*;

public class MyScoreQuery extends CustomScoreQuery {
	
	private Set<String> boostTerms;
	private Map<String, Double> boostMap;
	private double maxScore = 0;

	public MyScoreQuery(Query subQuery, List<ExpandableTerm> boosts) {
		super(subQuery);
		this.boostTerms = new HashSet<String>();
		this.boostMap = new HashMap<String, Double>();
		for (ExpandableTerm et : boosts) {
			boostTerms.add(et.getTerm());
			boostMap.put(et.getTerm(), et.getRelevanceScore());
			double curScore;
			if ((curScore = et.getRelevanceScore()) > maxScore) {
				maxScore = curScore;
			}
		}
	}
	
	@Override
    protected CustomScoreProvider getCustomScoreProvider(LeafReaderContext context) throws IOException {
		return new CustomScoreProvider(context) {
			@Override
			public float customScore(int doc, float subQueryScore, float valSrcScore) throws IOException {
				LeafReader reader = context.reader();
				Terms tv = reader.getTermVector(doc, CONTENT_FIELD);
				TermsEnum tEnum = tv.iterator();
				BytesRef term;
//				System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
//				System.out.println(reader.document(doc).get(DOCID_FIELD));
//				System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
				// For every term in document, check if there's a boost available for that term.
				// Normalize boost: divide it by maximum boost value.
				// Multiply normalized boosts to get total boost factor for this document.
				float totalBoost = 1;
				while ((term = tEnum.next()) != null) {
					String termAsString = term.utf8ToString();
					if (boostTerms.contains(termAsString)) {
						double curBoost = boostMap.get(termAsString);
						float curBoostNormalized = new Double(1 + curBoost/maxScore).floatValue();
//						System.out.println("curBoostNorm="+curBoostNormalized);
						totalBoost *= curBoostNormalized;
					}
				}
				float superCustomScore = super.customScore(doc, subQueryScore, valSrcScore);
//				System.out.println("super custom score: " + superCustomScore + ", total boost: " + totalBoost);
				return totalBoost * superCustomScore;
			}
		};
    }

}
