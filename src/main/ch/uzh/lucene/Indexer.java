package ch.uzh.lucene;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexOptions;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.RAMDirectory;

import static ch.uzh.main.Config.*;

// Code from "Lucene in Action" p. 12f
// and http://lingpipe-blog.com/2014/04/10/lucene4-document-classification/

public class Indexer {
	
	/**
	 * Indexes documents stored in a CSV file. Stores index on disk.
	 * 
	 * @param indexDir Where the index will be stored.
	 * @param csvFile One document per line. Structure: docID Label Content.
	 * @param goldstandard This file declares which documents in {@code csvFile} to index.
	 * All documents will be indexed if goldstandard is {@code null}.
	 * Structure: docID Judgment Highlight. Judgement: 'R' for relevant, 'N' for nonrelevant.
	 * For each highlight in a relevant document there is one line.
	 * @param docsToIndex Elements to index in goldstandard.
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public static void buildFSIndexFromCsv(File indexDir, File csvFile, File goldstandard, Set<Integer> docsToIndex)
			throws IOException, FileNotFoundException {
		
		Directory fsDir = FSDirectory.open(indexDir.toPath());
		// Analyzer (takes care of tokenization, stop word removal etc.)
		Reader stopwordsDe = new FileReader(STOPWORDS_DE_FILE);
		IndexWriterConfig iwConf = new IndexWriterConfig(new StandardAnalyzer(stopwordsDe));
		iwConf.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
		IndexWriter indexWriter = new IndexWriter(fsDir, iwConf);

		indexCsv(indexWriter, csvFile, goldstandard, docsToIndex);
		indexWriter.commit();
		indexWriter.close();
	}

	/**
	 * Indexes documents stored in a CSV file. Stores index on disk. Boosts individual documents.
	 * 
	 * @param indexDir Where the index will be stored.
	 * @param csvFile One document per line. Structure: docID Label Content.
	 * @param goldstandard This file declares which documents in {@code csvFile} to index.
	 * All documents will be indexed if goldstandard is {@code null}.
	 * Structure: docID Judgment Highlight. Judgement: 'R' for relevant, 'N' for nonrelevant.
	 * For each highlight in a relevant document there is one line.
	 * @param docsToIndex Elements to index in goldstandard.
	 * @param boostFactors Boost factors as key-value pairs with key = docID (which document to boost) and value = boost factor.
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public static void buildFSIndexFromCsv(File indexDir, File csvFile, File goldstandard, Set<Integer> docsToIndex, Map<String, Double> boostFactors)
			throws IOException, FileNotFoundException {
		
		Directory fsDir = FSDirectory.open(indexDir.toPath());
		// Analyzer (takes care of tokenization, stop word removal etc.)
		Reader stopwordsDe = new FileReader(STOPWORDS_DE_FILE);
		IndexWriterConfig iwConf = new IndexWriterConfig(new StandardAnalyzer(stopwordsDe));
		iwConf.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
		IndexWriter indexWriter = new IndexWriter(fsDir, iwConf);

		indexCsv(indexWriter, csvFile, goldstandard, docsToIndex, boostFactors);
		indexWriter.commit();
		indexWriter.close();
		
	}

	/**
	 * Indexes documents in a CSV file that are included in the goldstandard.
	 * If no goldstandard is available (goldstandard = {@code null}) all the documents in {@code csvFile} will be indexed.
	 * 
	 * @param indexWriter
	 * @param csvFile
	 * @param goldstandard
	 * @param docsToIndexGold Elements to index in goldstandard.
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	private static void indexCsv(IndexWriter indexWriter, File csvFile, File goldstandard, Set<Integer> docsToIndexGold) throws FileNotFoundException, IOException {
		Map<String, Document> docMap = mapAllDocuments(csvFile);
		if (goldstandard == null) {
			System.err.println("WARNING: No goldstandard defined, indexing all documents in CSV...");
			Set<Entry<String, Document>> set = docMap.entrySet();
			Iterator<Entry<String, Document>> iterator = set.iterator();
			while (iterator.hasNext()) {
				Map.Entry<String, Document> mapEntry = (Map.Entry<String, Document>) iterator.next();
				// add document to index
				indexWriter.addDocument(mapEntry.getValue());
			}
		} else {
			BufferedReader br = new BufferedReader(new FileReader(goldstandard));
			// set containing all docIDs of documents in the goldstandard (that are between goldstdStart and goldstdEnd)
			Set<String> docsToIndex = new HashSet<String>();
			String line;
		    int curLine = 1;
		    int curDoc = 0;
		    String prevDocId = "";
		    System.err.println("Indexing " + docsToIndexGold.size() + " documents in goldstandard...");
		    while ((line = br.readLine()) != null) {
		    	Pattern p = Pattern.compile("^(\\S*)[\\s,]*(\\S*)[\\s,]*(.*)$");
		    	Matcher m = p.matcher(line);
		    	if (!m.matches()) {
		    		System.out.println("Error at line " + curLine + " in goldstandard (no matches found): " + line);
				}
		    	String docId = m.group(1); @SuppressWarnings("unused") String relevance = m.group(2); @SuppressWarnings("unused") String highlight = m.group(3);
		    	// a new document occurs only if docId changes
		    	if (!prevDocId.equals(docId)) {
					curDoc++;
					prevDocId = docId;
				}
		    	// only index document if it's in indexing range
		    	if (docsToIndexGold.contains(curDoc)) {
		    		docsToIndex.add(docId);
				}
				curLine++;
		    }
		    br.close();
		    for (String docToIndex : docsToIndex) {
		    	// add document to index
				indexWriter.addDocument(docMap.get(docToIndex));
			}
		}
		System.err.println(indexWriter.numDocs() + " documents indexed.");
	}
	
	/**
	 * Indexes documents in a CSV file that are included in the goldstandard. Indexes ALL such files.
	 * If no goldstandard is available (goldstandard = {@code null}) all the documents in {@code csvFile} will be indexed.
	 * 
	 * @param indexWriter
	 * @param csvFile
	 * @param goldstandard
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	private static void indexCsv(IndexWriter indexWriter, File csvFile, File goldstandard) throws FileNotFoundException, IOException {
		Map<String, Document> docMap = mapAllDocuments(csvFile);
		if (goldstandard == null) {
			System.err.println("WARNING: No goldstandard defined, indexing all documents in CSV...");
			Set<Entry<String, Document>> set = docMap.entrySet();
			Iterator<Entry<String, Document>> iterator = set.iterator();
			while (iterator.hasNext()) {
				Map.Entry<String, Document> mapEntry = (Map.Entry<String, Document>) iterator.next();
				// add document to index
				indexWriter.addDocument(mapEntry.getValue());
			}
		} else {
			BufferedReader br = new BufferedReader(new FileReader(goldstandard));
			// set containing all docIDs of documents in the goldstandard (that are between goldstdStart and goldstdEnd)
			Set<String> docsToIndex = new HashSet<String>();
			String line;
		    int curLine = 1;
		    String prevDocId = "";
		    System.err.println("Indexing all documents in goldstandard...");
		    while ((line = br.readLine()) != null) {
		    	Pattern p = Pattern.compile("^(\\S*)[\\s,]*(\\S*)[\\s,]*(.*)$");
		    	Matcher m = p.matcher(line);
		    	if (!m.matches()) {
		    		System.out.println("Error at line " + curLine + " in goldstandard (no matches found): " + line);
				}
		    	String docId = m.group(1); @SuppressWarnings("unused") String relevance = m.group(2); @SuppressWarnings("unused") String highlight = m.group(3);
		    	// a new document occurs only if docId changes
		    	if (!prevDocId.equals(docId)) {
					prevDocId = docId;
				}
		    	docsToIndex.add(docId);
				curLine++;
		    }
		    br.close();
		    for (String docToIndex : docsToIndex) {
		    	// add document to index
				indexWriter.addDocument(docMap.get(docToIndex));
			}
		}
		System.err.println(indexWriter.numDocs() + " documents indexed.");
	}
	
	/**
	 * Indexes documents in a CSV file that are included in the goldstandard.
	 * If no goldstandard is available (goldstandard = {@code null}) all the documents in {@code csvFile} will be indexed.
	 * Documents in {@code boostFactors} will be boosted accordingly.
	 * 
	 * @param indexWriter
	 * @param csvFile
	 * @param goldstandard
	 * @param docsToIndexGold Elements to index in goldstandard.
	 * @param boostFactors
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	private static void indexCsv(IndexWriter indexWriter, File csvFile, File goldstandard, Set<Integer> docsToIndexGold, Map<String, Double> boostFactors) throws FileNotFoundException, IOException {
		Map<String, Document> docMap = mapAllDocuments(csvFile, boostFactors);
		if (goldstandard == null) {
			System.err.println("WARNING: No goldstandard defined, indexing all documents in CSV...");
			Set<Entry<String, Document>> set = docMap.entrySet();
			Iterator<Entry<String, Document>> iterator = set.iterator();
			while (iterator.hasNext()) {
				Map.Entry<String, Document> mapEntry = (Map.Entry<String, Document>) iterator.next();
				// add document to index
				indexWriter.addDocument(mapEntry.getValue());
			}
		} else {
			BufferedReader br = new BufferedReader(new FileReader(goldstandard));
			// set containing all docIDs of documents in the goldstandard (that are between goldstdStart and goldstdEnd)
			Set<String> docsToIndex = new HashSet<String>();
			String line;
		    int curLine = 1;
		    int curDoc = 0;
		    String prevDocId = "";
		    System.err.println("Indexing " + docsToIndexGold.size() + " documents in goldstandard...");
		    while ((line = br.readLine()) != null) {
		    	Pattern p = Pattern.compile("^(\\S*)[\\s,]*(\\S*)[\\s,]*(.*)$");
		    	Matcher m = p.matcher(line);
		    	if (!m.matches()) {
		    		System.out.println("Error at line " + curLine + " in goldstandard (no matches found): " + line);
				}
		    	String docId = m.group(1); @SuppressWarnings("unused") String relevance = m.group(2); @SuppressWarnings("unused") String highlight = m.group(3);
		    	// a new document occurs only if docId changes
		    	if (!prevDocId.equals(docId)) {
					curDoc++;
					prevDocId = docId;
				}
		    	// only index document if it's in indexing range
		    	if (docsToIndexGold.contains(curDoc)) {
		    		docsToIndex.add(docId);
				}
				curLine++;
		    }
		    br.close();
		    for (String docToIndex : docsToIndex) {
		    	// add document to index
				indexWriter.addDocument(docMap.get(docToIndex));
			}
		}
		System.err.println(indexWriter.numDocs() + " documents indexed.");
	}

	/**
	 * Creates a map of all documents in the {@code csvFile} data file. Key = docID, Value = LuceneDocument
	 * that can be indexed by calling IndexWriter.addDocument().
	 * 
	 * @param csvFile
	 * @return
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	private static Map<String, Document> mapAllDocuments(File csvFile) throws IOException, FileNotFoundException {
		BufferedReader br = new BufferedReader(new FileReader(csvFile));
		Map<String, Document> docMap = new HashMap<String, Document>();
		String line;
	    int curLine = 1;
	    while ((line = br.readLine()) != null) {
	    	Pattern p = Pattern.compile("^(\\S*)[\\s,]*(\\S*)[\\s,]*(.*)$");
	    	Matcher m = p.matcher(line);
	    	if (!m.matches()) {
	    		System.out.println("Error at line " + curLine + " in CSV (no matches found): " + line);
			}
	    	String docId = m.group(1); @SuppressWarnings("unused") String label = m.group(2); String content = m.group(3);
	    	Document doc = new Document();
	    	FieldType fieldType = new FieldType();
	    	fieldType.setStoreTermVectors(true);
	    	fieldType.setStored(true);
			fieldType.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS);
			Field docIdField = new Field(DOCID_FIELD, docId, fieldType);
			doc.add(docIdField);
			Field contentField = new Field(CONTENT_FIELD, content, fieldType);
			doc.add(contentField);
//	    	doc.add(new StringField(DOCID_FIELD, docId, Store.YES));
//			doc.add(new TextField(CONTENT_FIELD, content, Store.NO)); // we don't need to know the contents, just index the content 
			docMap.put(docId, doc);
			curLine++;
		}
	    br.close();
	    return docMap;
	}
	
	/**
	 * Creates a map of all documents in the {@code csvFile} data file. Key = docID, Value = LuceneDocument
	 * that can be indexed by calling IndexWriter.addDocument().
	 * Documents with docID in map {@code boostFactors} will be boosted at index time.
	 * 
	 * @param csvFile
	 * @param boostFactors
	 * @return
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	private static Map<String, Document> mapAllDocuments(File csvFile, Map<String, Double> boostFactors) throws IOException, FileNotFoundException {
		BufferedReader br = new BufferedReader(new FileReader(csvFile));
		Map<String, Document> docMap = new HashMap<String, Document>();
		String line;
	    int curLine = 1;
	    while ((line = br.readLine()) != null) {
	    	Pattern p = Pattern.compile("^(\\S*)[\\s,]*(\\S*)[\\s,]*(.*)$");
	    	Matcher m = p.matcher(line);
	    	if (!m.matches()) {
	    		System.out.println("Error at line " + curLine + " in CSV (no matches found): " + line);
			}
	    	String docId = m.group(1); @SuppressWarnings("unused") String label = m.group(2); String content = m.group(3);
	    	FieldType fieldType = new FieldType();
	    	fieldType.setStoreTermVectors(true);
	    	fieldType.setStored(true);
			fieldType.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS);
			Field docIdField = new Field(DOCID_FIELD, docId, fieldType);
			Field contentField = new Field(CONTENT_FIELD, content, fieldType);
//	    	StringField docIdField = new StringField(DOCID_FIELD, docId, Store.YES);
//	    	TextField contentField = new TextField(CONTENT_FIELD, content, Store.NO); // we don't need to know the contents, just index the content
	    	if (boostFactors.get(docId) != null) {
	    		Double boostFactor = boostFactors.get(docId);
	    		float boostFactorFloat = boostFactor.floatValue();
				contentField.setBoost(boostFactorFloat);
//				System.err.println("Using boost " + boostFactorFloat + " for doc " + docId);
			}
	    	Document doc = new Document();
	    	doc.add(docIdField);
			doc.add(contentField); 
			docMap.put(docId, doc);
			curLine++;
		}
	    br.close();
	    return docMap;
	}

	/**
	 * Indexes files in a directory. Filename is used as a document ID. Index will be stored on disk.
	 * 
	 * @param indexDir Where the index is stored.
	 * @param docsDir Directory containing the documents to be indexed.
	 * @return Number of documents indexed.
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public static int buildFSIndexFromDirectory(File indexDir, File docsDir)
			throws IOException, FileNotFoundException {
		
		// FSDirectory works with disk memory 
		Directory fsDir = FSDirectory.open(indexDir.toPath());
		// Analyzer (takes care of tokenization, stop word removal etc.)
		Reader stopwordsDe = new FileReader(STOPWORDS_DE_FILE);
		IndexWriterConfig iwConf = new IndexWriterConfig(new StandardAnalyzer(stopwordsDe));
		iwConf.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
		IndexWriter indexWriter = new IndexWriter(fsDir, iwConf);

		indexDirectory(indexWriter, docsDir);
		int numDocs = indexWriter.numDocs();
		indexWriter.commit();
		indexWriter.close();

		return numDocs;
	}
	
	/**
	 * Indexes documents stored in a CSV file. Keeps index in memory. Indexes ALL documents in goldstandard.
	 * 
	 * @param csvFile One document per line. Structure: docID Label Content.
	 * @param goldstandard This file declares which documents in {@code csvFile} to index.
	 * All documents will be indexed if goldstandard is {@code null}.
	 * Structure: docID Judgment Highlight. Judgement: 'R' for relevant, 'N' for nonrelevant.
	 * For each highlight in a relevant document there is one line.
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public static Directory buildRAMIndexFromCsv(File csvFile, File goldstandard)
			throws IOException, FileNotFoundException {
		
		Directory ramDir = new RAMDirectory();
		// Analyzer (takes care of tokenization, stop word removal etc.)
		Reader stopwordsDe = new FileReader(STOPWORDS_DE_FILE);
		IndexWriterConfig iwConf = new IndexWriterConfig(new StandardAnalyzer(stopwordsDe));
		iwConf.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
		IndexWriter indexWriter = new IndexWriter(ramDir, iwConf);

		indexCsv(indexWriter, csvFile, goldstandard);
		indexWriter.commit();
		indexWriter.close();
		
		return ramDir;
	}
	
	/**
	 * Indexes documents stored in a CSV file. Keeps index in memory.
	 * 
	 * @param csvFile One document per line. Structure: docID Label Content.
	 * @param goldstandard This file declares which documents in {@code csvFile} to index.
	 * All documents will be indexed if goldstandard is {@code null}.
	 * Structure: docID Judgment Highlight. Judgement: 'R' for relevant, 'N' for nonrelevant.
	 * For each highlight in a relevant document there is one line.
	 * @param docsToIndex Elements to index in goldstandard.
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public static Directory buildRAMIndexFromCsv(File csvFile, File goldstandard, Set<Integer> docsToIndex)
			throws IOException, FileNotFoundException {
		
		Directory ramDir = new RAMDirectory();
		// Analyzer (takes care of tokenization, stop word removal etc.)
		Reader stopwordsDe = new FileReader(STOPWORDS_DE_FILE);
		IndexWriterConfig iwConf = new IndexWriterConfig(new StandardAnalyzer(stopwordsDe));
		iwConf.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
		IndexWriter indexWriter = new IndexWriter(ramDir, iwConf);

		indexCsv(indexWriter, csvFile, goldstandard, docsToIndex);
		indexWriter.commit();
		indexWriter.close();
		
		return ramDir;
	}
	
	/**
	 * Indexes files in a directory. Filename is used as a document ID. Index will be kept in memory.
	 * 
	 * @param docsDir Directory containing the documents to be indexed.
	 * @return Number of documents indexed.
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public static Directory buildRAMIndexFromDirectory(File docsDir)
			throws IOException, FileNotFoundException {
		
		// RAMDirectory works with RAM (index will not be stored on disk)
		Directory ramDir = new RAMDirectory();
		// Analyzer (takes care of tokenization, stop word removal etc.)
		Reader stopwordsDe = new FileReader(STOPWORDS_DE_FILE);
		IndexWriterConfig iwConf = new IndexWriterConfig(new StandardAnalyzer(stopwordsDe));
		iwConf.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
		IndexWriter indexWriter = new IndexWriter(ramDir, iwConf);

		indexDirectory(indexWriter, docsDir);
		indexWriter.commit();
		indexWriter.close();

		return ramDir;
	}

	// recursive method that calls itself when it finds a directory
	private static void indexDirectory(IndexWriter writer, File dir)
			throws IOException {
		File[] files = dir.listFiles();
		for (int i = 0; i < files.length; i++) {
			File f = files[i];
			if (f.isDirectory()) {
				indexDirectory(writer, f);
			} else if (f.getName().endsWith(".txt")) {
				indexFile(writer, f);
			}
		}
	}

	// method to actually index a file using Lucene
	private static void indexFile(IndexWriter writer, File f)
			throws IOException {
		if (f.isHidden() || !f.exists() || !f.canRead()) {
			return;
		}
		System.out.println("Indexing " + f.getCanonicalPath());
		Document doc = new Document();
		doc.add(new TextField("contents", new FileReader(f)));
		doc.add(new StringField(DOCID_FIELD, f.getCanonicalPath(), Store.YES));
		writer.addDocument(doc);
	}

}
