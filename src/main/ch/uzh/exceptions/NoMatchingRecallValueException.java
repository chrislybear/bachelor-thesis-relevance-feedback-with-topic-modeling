package ch.uzh.exceptions;

public class NoMatchingRecallValueException extends Exception {
	
	public NoMatchingRecallValueException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public NoMatchingRecallValueException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public NoMatchingRecallValueException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public NoMatchingRecallValueException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public NoMatchingRecallValueException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
