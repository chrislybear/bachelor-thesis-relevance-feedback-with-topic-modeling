package ch.uzh.topicmodel;

import static ch.uzh.main.Config.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import cc.mallet.pipe.CharSequence2TokenSequence;
import cc.mallet.pipe.CharSequenceLowercase;
import cc.mallet.pipe.Pipe;
import cc.mallet.pipe.SerialPipes;
import cc.mallet.pipe.TokenSequence2FeatureSequence;
import cc.mallet.pipe.TokenSequenceRemoveStopwords;
import cc.mallet.pipe.iterator.CsvIterator;
import cc.mallet.types.InstanceList;
import ch.uzh.data.AssessableDocument;
import ch.uzh.data.ExpandableTerm;
import ch.uzh.data.JudgedDocument;

public abstract class TM {
	
	protected InstanceList instances = null;
	
	/**
	 * Initializes instances from {@code inputFile} by running it through pipes.
	 * @param inputFile
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 */
	public TM(String inputFile) throws UnsupportedEncodingException, FileNotFoundException {
		// Begin by importing documents from text to feature sequences
        ArrayList<Pipe> pipeList = new ArrayList<Pipe>();

        // Pipes: lowercase, tokenize, remove stopwords, map to features
        pipeList.add(new CharSequenceLowercase());
        pipeList.add(new CharSequence2TokenSequence(Pattern.compile("\\p{L}[\\p{L}\\p{P}]+\\p{L}")));
        pipeList.add(new TokenSequenceRemoveStopwords(new File(STOPWORDS_DE_FILE), "UTF-8", false, false, false));
        pipeList.add(new TokenSequence2FeatureSequence());
//        pipeList.add(new PrintInputAndTarget());

        instances = new InstanceList (new SerialPipes(pipeList));
        
		Reader fileReader = new InputStreamReader(new FileInputStream(new File(inputFile)), "UTF-8");
        instances.addThruPipe(new CsvIterator (fileReader, Pattern.compile("^(\\S*)[\\s,]*(\\S*)[\\s,]*(.*)$"),
        		3, 2, 1)); // data, label, name fields
	}
	
	/**
	 * Get boost factor from content of relevant documents.
	 * Boost factor is determined by the fraction of the dominant topic of the relevant documents in the document 
	 * and a topic amplifier (specified in {@link ch.uzh.main.Config#TOPIC_AMPLIFIER}).
	 * <p>
	 * E.g. if the dominant topic in the relevant documents is topic 7 and the document consists to 8% of topic 7, 
	 * then the boost factor will be 1.08. If the topic amplifier is 10, then the boost factor will be 1 + 10*0.08 = 1.8.
	 * </p><p>
	 * Boost factor can be used to boost documents to improve aspectual retrieval.
	 * </p><p>
	 * Used for experiment<br>
	 * a1-b1   (document-documentboost)    -> .setBoost at indexing time
	 * </p>
	 * 
	 * @param relevanceFeedback Relevant documents with content (nonrelevant documents will be ignored, 
	 * docID and highlights may be null; however content and relevance must not be null).
	 * Entire content will be used to find dominant topic.
	 * @param documentsToAssess Documents for which a boost factor should be calculated. Content must not be null!
	 * @return Map of docID (key) and boost factor (value).
	 */
	public abstract Map<String, Double> getBoostFactorsFromEntireContents(List<JudgedDocument> relevanceFeedback, List<AssessableDocument> documentsToAssess);
	
	/**
	 * Get boost factor from highlighted text passages.
	 * Boost factor is determined by the fraction of the dominant topic of the highlights in the document 
	 * and a topic amplifier (specified in {@link ch.uzh.main.Config#TOPIC_AMPLIFIER}).
	 * <p>
	 * E.g. if the dominant topic in the highlights is topic 4 and the document consists to 12% of topic 4, 
	 * then the boost factor will be 1.12). If the topic amplifier is 10, then the boost factor will be 1 + 10*0.12 = 2.2.
	 * </p><p>
	 * Boost factor can be used to boost documents to improve aspectual retrieval.
	 * <p>
	 * Used for experiment<br>
	 * a3-b1   (highlight-documentboost)    -> .setBoost at indexing time
	 * </p>
	 * 
	 * @param relevanceFeedback Relevant documents with highlighted text passages (nonrelevant documents will be ignored, 
	 * content and docID may be null; however highlights and relevance must not be null).
	 * Highlights will be used to find dominant topic.
	 * @param documentsToAssess Documents for which a boost factor should be calculated. Content must not be null! 
	 * @return Map of docID (key) and boost factor (value).
	 */
	public abstract Map<String, Double> getBoostFactorsFromHighlights(List<JudgedDocument> relevanceFeedback, List<AssessableDocument> documentsToAssess);
	
	/**
	 * Get terms from same topics as content of relevant documents.
	 * Those terms can be used for query expansion.
	 * <p>
	 * Used for experiments<br>
	 * a1-b2.1	(document-queryexpansion)<br>
	 * a1-b2.2 (document-customscore) -> custom score at retrieval time
	 * </p>
	 * 
	 * @param relevanceFeedback Relevant documents with content (nonrelevant documents will be ignored, 
	 * docID and highlights may be null; however content and relevance must not be null).
	 * Entire content will be used to find dominant topic.
	 * @param numTerms Number of terms that are returned.
	 * @return Terms from dominant topic of {@code relevantDocData} and their relevance to the topic.
	 */
	public abstract List<ExpandableTerm> getSimilarTermsFromEntireContents(List<JudgedDocument> relevanceFeedback, int numTerms);

	/**
	 * Get terms from same topics as highlighted passages.
	 * Those terms can be used for query expansion.
	 * <p>
	 * Used for experiments<br>
	 * a3-b2.1	(highlight-queryexpansion)<br>
	 * a3-b2.2 (highlight-customscore) -> custom score at retrieval time
	 * </p>
	 * 
	 * @param relevanceFeedback Relevant documents with highlighted text passages (nonrelevant documents will be ignored, 
	 * content and docID may be null; however highlights and relevance must not be null).
	 * Highlights will be used to find dominant topic.
	 * @return Terms from dominant topic of {@code relevanceFeedback} and their relevance to the topic.
	 */
	public abstract List<ExpandableTerm> getSimilarTermsFromHighlights(List<JudgedDocument> relevanceFeedback, int numTerms);
	
}