package ch.uzh.topicmodel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cc.mallet.topics.ParallelTopicModel;
import cc.mallet.topics.TopicInferencer;
import cc.mallet.types.Alphabet;
import cc.mallet.types.FeatureSequence;
import cc.mallet.types.IDSorter;
import cc.mallet.types.Instance;
import cc.mallet.types.InstanceList;
import cc.mallet.types.LabelSequence;
import ch.uzh.data.AssessableDocument;
import ch.uzh.data.ExpandableTerm;
import ch.uzh.data.JudgedDocument;

import static ch.uzh.main.Config.*;

public class TMParallelLDA extends TM {
	
	private ParallelTopicModel model = null;
	
	/**
	 * Initializes instances from {@code inputFile} by running it through pipes.
	 * 
	 * @param inputFile
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 */
	public TMParallelLDA(String inputFile) throws UnsupportedEncodingException, FileNotFoundException {
		super(inputFile);
	}
	
	/**
	 * Run parallel LDA with random seed (non-deterministic).
	 * 
	 * @param numTopics Number of topics (e.g. 100).
	 * @param alphaSum_t Sum over all alphas. E.g. with {@code numTopics}=100 and {@code alphaSum_t}=1.0 the alpha will be 0.01 for each topic
	 * @param beta_w Beta (e.g. 0.01).
	 * @param iterations Number of iterations. For real applications use 1000 to 2000 iterations.
	 * @param stateDir Directory, where state of a previously trained topic model is stored. If there is no saved state found in {@code stateDir},
	 * a new topic model is built from scratch and its state saved under {@code stateDir}. Must not be {@code null}.
	 * @throws IOException
	 */
	public void runEstimation(int numTopics, double alphaSum_t, double beta_w, int iterations, String stateDir) throws IOException {
		runEstimation(numTopics, alphaSum_t, beta_w, iterations, stateDir, -1);
	}

	/**
	 * Run parallel LDA with fixed seed (deterministic).
	 * 
	 * @param numTopics Number of topics (e.g. 100).
	 * @param alphaSum_t Sum over all alphas. E.g. with {@code numTopics}=100 and {@code alphaSum_t}=1.0 the alpha will be 0.01 for each topic
	 * @param beta_w Beta (e.g. 0.01).
	 * @param iterations Number of iterations. For real applications use 1000 to 2000 iterations.
	 * @param stateDir Directory, where state of a previously trained topic model is stored. If there is no saved state found in {@code stateDir},
	 * a new topic model is built from scratch and its state saved under {@code stateDir}. Must not be {@code null}.
	 * @param seed Seed to start first iteration with. Using {@code seed}=-1 will randomize the process and yield non-deterministic results.
	 * @throws IOException
	 */
	public void runEstimation(int numTopics, double alphaSum_t, double beta_w, int iterations, String stateDir, int seed) throws IOException {
	    //  Note that the first parameter is passed as the sum over topics, while
	    //  the second is the parameter for a single dimension of the Dirichlet prior.
	    model = new ParallelTopicModel(numTopics, alphaSum_t, beta_w);
	
	    model.setRandomSeed(seed);
	    model.addInstances(instances);
	
	    // Use parallel samplers, which each look at 1/NUM_THREADS of the corpus and combine statistics after every iteration.
//	    int numberOfCores = Runtime.getRuntime().availableProcessors();
//	    System.out.println("Found " + numberOfCores + " cores on this system. Using " + numberOfCores + " threads to run LDA.");
//	    int numThreads = numberOfCores;
	    int numThreads = NUM_THREADS;
	    
	    model.setNumThreads(numThreads);
		System.out.println("Using " + numThreads  + " threads to train topic model.");
	    
	    // search for saved states and load topic model from there
	    initializeSavedState(iterations, stateDir);
	    
	    
	    // --------------------------------------------------
/*
	    // Show the words and topics in the first instance
	    
	    // The data alphabet maps word IDs to strings
	    Alphabet dataAlphabet = instances.getDataAlphabet();
	    
	    FeatureSequence tokens = (FeatureSequence) model.getData().get(0).instance.getData();
	    LabelSequence topics = model.getData().get(0).topicSequence;
	    
	    Formatter out = new Formatter(new StringBuilder(), Locale.US);
	    for (int position = 0; position < tokens.getLength(); position++) {
	        out.format("%s<-%d ", dataAlphabet.lookupObject(tokens.getIndexAtPosition(position)), topics.getIndexAtPosition(position));
	    }
	    System.out.println("Words and the topic, from which they were picked, of the first instance (same word can come from several topics):");
	    System.out.println(out);
	    
	    // --------------------------------------------------
	    
	    // Estimate the topic distribution of the first instance, 
	    //  given the current Gibbs state.
	    double[] topicDistribution = model.getTopicProbabilities(0);
	
	    // Get an array of sorted sets of word ID/count pairs
	    ArrayList<TreeSet<IDSorter>> topicSortedWords = model.getSortedWords();
	    
	    // Show top 5 words in topics with proportions for the first document
	    System.out.println("\nTopic distribution of first instance. For each topic the 5 most significant words are shown (weight/tf in parantheses):");
	    for (int topic = 0; topic < numTopics; topic++) {
	        Iterator<IDSorter> iterator = topicSortedWords.get(topic).iterator();
	        
	        out = new Formatter(new StringBuilder(), Locale.US);
	        out.format("%d\t%.3f\t", topic, topicDistribution[topic]);
	        int rank = 0;
	        while (iterator.hasNext() && rank < 5) {
	            IDSorter idCountPair = iterator.next();
	            out.format("%s (%.0f) ", dataAlphabet.lookupObject(idCountPair.getID()), idCountPair.getWeight());
	            rank++;
	        }
	        System.out.println(out);
	    }
	    
	    // Find highest probability
	    double maxValue = 0.0;
	    int maxIndex = 0;
	    for (int topic = 0; topic < numTopics; topic++) {
			if (topicDistribution[topic] > maxValue) {
				maxValue = topicDistribution[topic];
				maxIndex = topic;
			}
		}
	    System.out.println("Instance 1 (" + instances.get(0).getName() + ") most probably belongs to topic " + maxIndex + " (probability: " + String.format("%.3f", maxValue) + ")");
	    
	    // --------------------------------------------------
	    
	    // Create a new instance with high probability of topic 0
	    System.out.println("\nCreating a new instance with high probability of topic 0:");
	    StringBuilder topicZeroText = new StringBuilder();
	    Iterator<IDSorter> iterator = topicSortedWords.get(0).iterator();
	
	    int rank = 0;
	    while (iterator.hasNext() && rank < 5) {
	        IDSorter idCountPair = iterator.next();
	        topicZeroText.append(dataAlphabet.lookupObject(idCountPair.getID()) + " ");
	        rank++;
	    }
	    
	    System.out.println('"' + topicZeroText.toString() + '"');
	
	    // Create a new instance named "test instance" with empty target and source fields.
	    InstanceList testing = new InstanceList(instances.getPipe());
	    testing.addThruPipe(new Instance(topicZeroText.toString(), null, "test instance", null));
	
	    TopicInferencer inferencer = model.getInferencer();
	    double[] testProbabilities = inferencer.getSampledDistribution(testing.get(0), 10, 1, 5);
	    System.out.println("0\t" + testProbabilities[0]);
*/
	}

	private void initializeSavedState(int iterations, String stateDir) throws IOException {
		File tmStateDir = new File(stateDir);
	    String tmStateFilePath = stateDir + File.separator + TM_STATE_FILENAME;
	    if (tmStateDir.exists() && tmStateDir.isDirectory()) {
	    	// find maximum state
	    	String[] fileList = tmStateDir.list();
			Pattern p = Pattern.compile("^(" + TM_STATE_FILENAME + ".*)\\.(\\d+)$");
	    	int maxState = 0;
	    	int numMatches = 0;
	    	String fileName = "";
	    	for (String file : fileList) {
	    		File f = new File(stateDir + File.separator + file);
	    		if (f.isFile() && !f.isHidden()) {
	    			Matcher m = p.matcher(file);
			    	if (m.matches()) {
			    		numMatches++;
			    		fileName = m.group(1); int curState = Integer.parseInt(m.group(2));
			    		if (curState > maxState) {
							maxState = curState;
			    		}
					}
				}
	    	}
	    	if (numMatches <= 0) {
	    		System.out.println("No state found. Building topic model from scratch... (saving states in " + stateDir + ")");
	    		model.setNumIterations(iterations);
	    		model.setSaveState(10, tmStateFilePath);
	    		model.estimate();
	    	} else if (maxState >= iterations) {
	    		System.out.println("Initializing topic model from state '" + fileName + "." + maxState + "'...");
	    		model.initializeFromState(new File(stateDir + File.separator + fileName + "." + maxState));
			} else {
				System.out.println("State found at iteration " + maxState + ". Improving topic model until " + iterations + " iterations...");
				System.out.println("NOTE: Filenames will be of the form '" + TM_STATE_FILENAME + maxState + "+.XX' "
						+ "where XX are the iterations counted from 0. You might want to rename the iteration numbers manually after estimation.");
	    		model.setNumIterations(iterations - maxState);
	    		model.setSaveState(10, tmStateFilePath + maxState + "+");
	    		model.estimate();
			}
	    } else {
	    	System.out.println("No state found. Building topic model from scratch... (saving states in " + stateDir + ")");
	    	if (!tmStateDir.mkdirs()) {
	    		System.out.println("DIR COULD NOT BE CREATED!");
	    	}
	    	if (!tmStateDir.canWrite()) {
	    		System.out.println("NO WRITE PERMISSION!");
	    	}
	    	model.setNumIterations(iterations);
    		model.setSaveState(10, tmStateFilePath);
	    	model.estimate();
	    }
	}

	// a1-b1 (document-documentboost)    -> .setBoost at indexing time
	public Map<String, Double> getBoostFactorsFromEntireContents(List<JudgedDocument> relevanceFeedback, List<AssessableDocument> documentsToAssess) {
		
		Map<String, Double> boostFactors = new HashMap<String, Double>();
		
		// build new document from all relevant documents, call it "Entire Content Instance" (target and source fields are empty)
		StringBuilder stringBuilder = new StringBuilder();
		for (JudgedDocument judgedDocument : relevanceFeedback) {
			stringBuilder.append(judgedDocument.getContent() + " ");
		}
	    InstanceList testInstances = new InstanceList(instances.getPipe());
	    testInstances.addThruPipe(new Instance(stringBuilder.toString(), null, "Entire Content Instance", null));
	    
	    // infer topic distribution of new document
	    TopicInferencer inferencer = model.getInferencer();
	    double[] testProbabilities = inferencer.getSampledDistribution(testInstances.get(0), 0, 1, 5);
	    
	    // show topic distribution
//	    for (int topic = 0; topic < testProbabilities.length; topic++) {
//	    	System.out.println(topic + "\t" + String.format("%.3f", testProbabilities[topic]));
//		}
	    
	    // find topic with highest probability
	    double maxValue = 0.0;
	    int maxTopic = 0;
	    for (int topic = 0; topic < testProbabilities.length; topic++) {
			if (testProbabilities[topic] > maxValue) {
				maxValue = testProbabilities[topic];
				maxTopic = topic;
			}
		}
	    System.out.println("Instance '" + testInstances.get(0).getName() + "' most probably belongs to topic " + maxTopic
	    		+ " (probability: " + String.format("%.3f", maxValue) + "). Calculating boost factors according to distribution of topic " + maxTopic + "...");
	    
	    
	    // run inferencer on documentsToAssess (i.e. get topic distribution of maxTopic in all documents in documentsToAssess)
	    
	    // run all assessable documents through pipe
	    InstanceList assessInstances = new InstanceList(instances.getPipe());
	    for (AssessableDocument assessDoc : documentsToAssess) {
	    	assessInstances.addThruPipe(new Instance(assessDoc.getContent(), null, assessDoc.getDocID(), null));
		}
	    
	    // get topic fraction of dominant topic in relevance feedback and use it as boost factor
	    double[] currentInstanceProbabilities;
	    for (Instance assessInstance : assessInstances) {
			currentInstanceProbabilities = inferencer.getSampledDistribution(assessInstance, 0, 1, 5);
			
//			System.out.println("\nTopic distribution of topic " + maxTopic + " in document '" + assessInstance.getName() + "': " + String.format("%.2f", currentInstanceProbabilities[maxTopic]));
//			for (double d : currentInstanceProbabilities) {
//				System.err.print(String.format("%.3f", d) + " ");
//			}
			
			boostFactors.put((String) assessInstance.getName(), 1 + TOPIC_AMPLIFIER*currentInstanceProbabilities[maxTopic]);
		}
        
        return boostFactors;
	}
	
	// a3-b1 (highlight-documentboost)    -> .setBoost at indexing time
	public Map<String, Double> getBoostFactorsFromHighlights(List<JudgedDocument> relevanceFeedback, List<AssessableDocument> documentsToAssess) {
		
		Map<String, Double> boostFactors = new HashMap<String, Double>();
		
		// build new document from all highlights, call it "Highlight Instance" (target and source fields are empty)
		StringBuilder stringBuilder = new StringBuilder();
		for (JudgedDocument judgedDocument : relevanceFeedback) {
//			System.out.print(judgedDocument.getDocID() + "\t");
			for (String highlight : judgedDocument.getHighlights()) {
				stringBuilder.append(highlight + " ");
//				System.out.print(highlight + " ");
			}
//			System.out.print("\n");
		}
	    InstanceList testInstances = new InstanceList(instances.getPipe());
	    testInstances.addThruPipe(new Instance(stringBuilder.toString(), null, "Highlight Instance", null));
	    
	    // infer topic distribution of new document
	    TopicInferencer inferencer = model.getInferencer();
	    double[] testProbabilities = inferencer.getSampledDistribution(testInstances.get(0), 0, 1, 5);
	    
	    // show topic distribution
//	    for (int topic = 0; topic < testProbabilities.length; topic++) {
//	    	System.out.println(topic + "\t" + String.format("%.3f", testProbabilities[topic]));
//		}
	    
	    // find topic with highest probability
	    double maxValue = 0.0;
	    int maxTopic = 0;
	    for (int topic = 0; topic < testProbabilities.length; topic++) {
			if (testProbabilities[topic] > maxValue) {
				maxValue = testProbabilities[topic];
				maxTopic = topic;
			}
		}
	    System.out.println("Instance '" + testInstances.get(0).getName() + "' most probably belongs to topic " + maxTopic
	    		+ " (probability: " + String.format("%.3f", maxValue) + "). Calculating boost factors considering distribution of topic " + maxTopic + "...");
	    
	    
	    
	    // run inferencer on documentsToAssess (i.e. get topic distribution of maxTopic in all documents in documentsToAssess)
	    
	    // run all assessable documents through pipe
	    InstanceList assessInstances = new InstanceList(instances.getPipe());
	    for (AssessableDocument assessDoc : documentsToAssess) {
	    	assessInstances.addThruPipe(new Instance(assessDoc.getContent(), null, assessDoc.getDocID(), null));
		}
	    
	    // get topic fraction of dominant topic in relevance feedback and use it as boost factor
	    double[] currentInstanceProbabilities;
	    for (Instance assessInstance : assessInstances) {
			currentInstanceProbabilities = inferencer.getSampledDistribution(assessInstance, 0, 1, 5);
			
//			System.out.println("\nTopic distribution of topic " + maxTopic + " in document '" + assessInstance.getName() + "': " + String.format("%.2f", currentInstanceProbabilities[maxTopic]));
//			for (double d : currentInstanceProbabilities) {
//				System.err.print(String.format("%.3f", d) + " ");
//			}
			boostFactors.put((String) assessInstance.getName(), 1 + TOPIC_AMPLIFIER*currentInstanceProbabilities[maxTopic]);
		}
        
        return boostFactors;
	}
	
	// a1-b2.1 (document-queryexpansion)
	// a1-b2.2 (document-customscore) -> custom score at retrieval time
		public List<ExpandableTerm> getSimilarTermsFromEntireContents(List<JudgedDocument> relevanceFeedback, int numTerms) {
			
			List<ExpandableTerm> expandableTerms = new ArrayList<ExpandableTerm>();
			
			// build new document from all relevant documents, call it "Entire Content Instance" (target and source fields are empty)
			StringBuilder stringBuilder = new StringBuilder();
			for (JudgedDocument judgedDocument : relevanceFeedback) {
				stringBuilder.append(judgedDocument.getContent() + " ");
			}
		    InstanceList testInstances = new InstanceList(instances.getPipe());
		    testInstances.addThruPipe(new Instance(stringBuilder.toString(), null, "Entire Content Instance", null));
		    
		    // infer topic distribution of new document
		    TopicInferencer inferencer = model.getInferencer();
		    double[] testProbabilities = inferencer.getSampledDistribution(testInstances.get(0), 0, 1, 5);
		    
		    // show topic distribution
	//	    for (int topic = 0; topic < testProbabilities.length; topic++) {
	//	    	System.out.println(topic + "\t" + String.format("%.3f", testProbabilities[topic]));
	//		}
		    
		    // find topic with highest probability
		    double maxValue = 0.0;
		    int maxTopic = 0;
		    for (int topic = 0; topic < testProbabilities.length; topic++) {
				if (testProbabilities[topic] > maxValue) {
					maxValue = testProbabilities[topic];
					maxTopic = topic;
				}
			}
		    System.out.println("Instance '" + testInstances.get(0).getName() + "' most probably belongs to topic " + maxTopic
		    		+ " (probability: " + String.format("%.3f", maxValue) + "). Getting " + numTerms + " most common words from topic " + maxTopic);
		    
		    // gather most dominant words from most probable topic
		    ArrayList<TreeSet<IDSorter>> topicSortedWords = model.getSortedWords(); // get an array of sorted sets of word ID/count pairs
		    Alphabet dataAlphabet = instances.getDataAlphabet(); // the data alphabet maps word IDs to strings
	        Iterator<IDSorter> iterator = topicSortedWords.get(maxTopic).iterator();
	        int rank = 0;
	        while (iterator.hasNext() && rank < numTerms) {
	            IDSorter idCountPair = iterator.next();
	            String word = (String) dataAlphabet.lookupObject(idCountPair.getID());
	            double weight = idCountPair.getWeight();
	            expandableTerms.add(new ExpandableTerm(word, weight));
	            rank++;
	        }
		    
			return expandableTerms;
		}

	// a3-b2.1 (highlight-queryexpansion)
	// a3-b2.2 (highlight-customscore) -> custom score at retrieval time
	public List<ExpandableTerm> getSimilarTermsFromHighlights(List<JudgedDocument> relevanceFeedback, int numTerms) {
		
		List<ExpandableTerm> expandableTerms = new ArrayList<ExpandableTerm>();
		
		// build new document from all highlights, call it "Highlight Instance" (target and source fields are empty)
		StringBuilder stringBuilder = new StringBuilder();
		for (JudgedDocument judgedDocument : relevanceFeedback) {
//			System.out.print(judgedDocument.getDocID() + "\t");
			for (String highlight : judgedDocument.getHighlights()) {
				stringBuilder.append(highlight + " ");
//				System.out.print(highlight + " ");
			}
//			System.out.print("\n");
		}
	    InstanceList testInstances = new InstanceList(instances.getPipe());
	    testInstances.addThruPipe(new Instance(stringBuilder.toString(), null, "Highlight Instance", null));
	    
	    // infer topic distribution of new document
	    TopicInferencer inferencer = model.getInferencer();
	    double[] testProbabilities = inferencer.getSampledDistribution(testInstances.get(0), 0, 1, 5);
	    
	    // show topic distribution
//	    for (int topic = 0; topic < testProbabilities.length; topic++) {
//	    	System.out.println(topic + "\t" + String.format("%.3f", testProbabilities[topic]));
//		}
	    
	    // find topic with highest probability
	    double maxValue = 0.0;
	    int maxTopic = 0;
	    for (int topic = 0; topic < testProbabilities.length; topic++) {
			if (testProbabilities[topic] > maxValue) {
				maxValue = testProbabilities[topic];
				maxTopic = topic;
			}
		}
	    System.out.println("Instance '" + testInstances.get(0).getName() + "' most probably belongs to topic " + maxTopic
	    		+ " (probability: " + String.format("%.3f", maxValue) + "). Getting " + numTerms + " most common words from topic " + maxTopic);
	    
	    // gather most dominant words from most probable topic
	    ArrayList<TreeSet<IDSorter>> topicSortedWords = model.getSortedWords(); // get an array of sorted sets of word ID/count pairs
	    Alphabet dataAlphabet = instances.getDataAlphabet(); // the data alphabet maps word IDs to strings
        Iterator<IDSorter> iterator = topicSortedWords.get(maxTopic).iterator();
        int rank = 0;
        while (iterator.hasNext() && rank < numTerms) {
            IDSorter idCountPair = iterator.next();
            String word = (String) dataAlphabet.lookupObject(idCountPair.getID());
            double weight = idCountPair.getWeight();
            expandableTerms.add(new ExpandableTerm(word, weight));
            rank++;
        }
	    
		return expandableTerms;
	}
	
}