package ch.uzh.topicmodel;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

import cc.mallet.topics.NPTopicModel;
import ch.uzh.data.AssessableDocument;
import ch.uzh.data.ExpandableTerm;
import ch.uzh.data.JudgedDocument;

public class TMNonParametric extends TM {
	
	private NPTopicModel model = null;
	
	public TMNonParametric(String inputFile) throws UnsupportedEncodingException, FileNotFoundException {
		super(inputFile);
	}
	
	// non parametric topic models don't require the number of topics to be predefined
		public void runEstimation() throws IOException {
			// TODO: find sensible values for alpha, gamma, beta
			model = new NPTopicModel(1, 1, NPTopicModel.DEFAULT_BETA);
	        model.addInstances(instances, 100);
	        model.sample(50);
	        // TODO: what does topWords do?
	        model.topWords(5);
	//		modelNP.printState(System.out);
		}

	// TODO: a1-b1		(document-documentboost)    -> .setBoost at indexing time
	//       a1-b2.2	(document-customsimilarity) -> custom similarity at retrieval time
	public Map<String, Double> getBoostFactorsFromEntireContents(List<JudgedDocument> relevanceFeedback, List<AssessableDocument> documentsToAssess) {
		return null;
	}
	
	// TODO: a3-b1		(highlight-documentboost)    -> .setBoost at indexing time
	//       a3-b2.2	(highlight-customsimilarity) -> custom similarity at retrieval time
	public Map<String, Double> getBoostFactorsFromHighlights(List<JudgedDocument> relevanceFeedback, List<AssessableDocument> documentsToAssess) {
		return null;
	}
	
	// TODO: a1-b2.1	(document-queryexpansion)
	public List<ExpandableTerm> getSimilarTermsFromEntireContents(List<JudgedDocument> relevanceFeedback, int numTerms) {
		return null;
	}

	// TODO: a3-b2.1	(highlight-queryexpansion)
	public List<ExpandableTerm> getSimilarTermsFromHighlights(List<JudgedDocument> relevanceFeedback, int numTerms) {
		return null;
	}
	
}