package ch.uzh.topicmodel;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

import cc.mallet.topics.SimpleLDA;
import cc.mallet.util.Randoms;
import ch.uzh.data.AssessableDocument;
import ch.uzh.data.ExpandableTerm;
import ch.uzh.data.JudgedDocument;

public class TMSimpleLDA extends TM {
	
	private SimpleLDA model = null;
	
	public TMSimpleLDA(String inputFile) throws UnsupportedEncodingException, FileNotFoundException {
		super(inputFile);
	}
	
	/**
	 * This implementation of LDA is deterministic, i.e. using the same seed will yield same results (if other parameters stay the same too).
	 * @param numTopics Number of topics (e.g. 100).
	 * @param alphaSum_t Sum over all alphas. E.g. with {@code numTopics}=100 and {@code alphaSum_t}=1.0 the alpha will be 0.01 for each topic
	 * @param beta_w Beta (e.g. 0.01).
	 * @param iterations Number of iterations. For real applications use 1000 to 2000 iterations.
	 * @param seed Seed to start first iteration with. Same seed will yield consistent results (even for -1).
	 * @throws IOException
	 */
	public void runEstimation(int numTopics, double alphaSum_t, double beta_w, int iterations, int seed) throws IOException {
		Randoms random = new Randoms(seed);
		model = new SimpleLDA(numTopics, alphaSum_t, beta_w, random);
        model.addInstances(instances);
        model.sample(iterations);
	}
	
	// TODO: a1-b1		(document-documentboost)    -> .setBoost at indexing time
	//       a1-b2.2	(document-customsimilarity) -> custom similarity at retrieval time
	public Map<String, Double> getBoostFactorsFromEntireContents(List<JudgedDocument> relevanceFeedback, List<AssessableDocument> documentsToAssess) {
		return null;
	}
	
	// TODO: a3-b1		(highlight-documentboost)    -> .setBoost at indexing time
	//       a3-b2.2	(highlight-customsimilarity) -> custom similarity at retrieval time
	public Map<String, Double> getBoostFactorsFromHighlights(List<JudgedDocument> relevanceFeedback, List<AssessableDocument> documentsToAssess) {
		return null;
	}
	
	// TODO: a1-b2.1	(document-queryexpansion)
	public List<ExpandableTerm> getSimilarTermsFromEntireContents(List<JudgedDocument> relevanceFeedback, int numTerms) {
		return null;
	}

	// TODO: a3-b2.1	(highlight-queryexpansion)
	public List<ExpandableTerm> getSimilarTermsFromHighlights(List<JudgedDocument> relevanceFeedback, int numTerms) {
		return null;
	}
	
}