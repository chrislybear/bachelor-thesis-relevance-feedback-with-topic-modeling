package ch.uzh.data;

import java.util.List;

public class JudgedDocument {
	
	String docID;
	boolean isRelevant;
	String content;
	List<String> highlights;
	
	public JudgedDocument(String docID, boolean isRelevant, String content, List<String> highlights) {
		this.docID = docID;
		this.isRelevant = isRelevant;
		this.content = content;
		this.highlights = highlights;
	}

	public String getDocID() {
		return docID;
	}

	public boolean isRelevant() {
		return isRelevant;
	}
	
	public String getContent() {
		return content;
	}
	
	public List<String> getHighlights() {
		return highlights;
	}

}
