package ch.uzh.data;

public class ExpandableTerm {

	private String term;
	private double relevanceScore;
	
	public ExpandableTerm(String term, double weight) {
		this.term = term;
		this.relevanceScore = weight;
	}

	public String getTerm() {
		return term;
	}

	public double getRelevanceScore() {
		return relevanceScore;
	}
	
}
