package ch.uzh.data;

public class AssessableDocument {

	String docID;
	String content;
	
	public AssessableDocument(String docID, String content) {
		this.docID = docID;
		this.content = content;
	}

	public String getDocID() {
		return docID;
	}

	public String getContent() {
		return content;
	}
	
}
