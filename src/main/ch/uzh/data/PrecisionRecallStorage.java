package ch.uzh.data;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class PrecisionRecallStorage implements Iterable<Entry<Integer, PrecisionRecallStorage.PrecisionRecallPair<Float, Float>>> {

	private Map<Integer, PrecisionRecallPair<Float, Float>> storage;

	public PrecisionRecallStorage() {
		storage = new HashMap<Integer, PrecisionRecallPair<Float, Float>>();
	}

	public void add(Integer k, Float recall, Float precision) {
		PrecisionRecallPair<Float, Float> pair = new PrecisionRecallPair<>(recall, precision);
		if (storage.containsKey(k)) {
			System.err.println("WARNING: " + this.getClass().getSimpleName() + " already contains a value at position " + k + "."
					+ " Overwriting " + storage.get(k) + " with " + pair + "!");
		}
		storage.put(k, pair);
	}
	
	public PrecisionRecallPair<Float, Float> at(Integer k) {
		return storage.get(k);
	}
	
	public int size() {
		return storage.size();
	}

	// Returns just pairs (no k).
	@Override
	public Iterator<Entry<Integer, PrecisionRecallPair<Float, Float>>> iterator() {
		return storage.entrySet().iterator();
	}
	
	@Override
	public String toString() {
		String str = "";
		for (Entry<Integer, PrecisionRecallPair<Float, Float>> entry : this) {
			int k = entry.getKey();
			str += "(" + k + ", " + entry.getValue().getRecall() + ", " + entry.getValue().getPrecision() + ") ";
		}
		return str.trim();
	}

	public class PrecisionRecallPair<R, P> {

		private final R recall;
		private final P precision;

		public PrecisionRecallPair(R recall, P precision) {
			this.recall = recall;
			this.precision = precision;
		}

		public R getRecall() {
			return recall;
		}

		public P getPrecision() {
			return precision;
		}

		@Override
		public int hashCode() {
			return recall.hashCode() ^ precision.hashCode();
		}

		@Override
		public boolean equals(Object o) {
			if (!(o instanceof PrecisionRecallPair))
				return false;
			PrecisionRecallPair<?, ?> pairo = (PrecisionRecallPair<?, ?>) o;
			return this.recall.equals(pairo.getRecall()) && this.precision.equals(pairo.getPrecision());
		}
		
		@Override
		public String toString() {
			return "(" + this.recall + ", " + this.precision + ")";
		}

	}

}
