package ch.uzh.data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.lucene.store.Directory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import ch.uzh.exceptions.InvalidRelevanceException;
import ch.uzh.lucene.Indexer;
import ch.uzh.lucene.Searcher;

public class DataUtils {

	/**
	 * Stores XML in a file that can be fed to the TopicModel.
	 * Uses {@code xmlFile} filename as name.
	 * 
	 * @param xmlFile
	 * @param outfile
	 * @throws IOException
	 */
	public static void processXml(String xmlFile, String outfile) throws IOException {
		File inputFile = new File(xmlFile);
		String fileName = inputFile.getName().replaceAll(".xml", "");
		processXml(xmlFile, outfile, fileName);
	}
	
	/**
	 * Stores XML in a file that can be fed to the TopicModel.
	 * 
	 * @param xmlFile Input file to be processed.
	 * @param outfile Output file to store the processed data in.
	 * @param name Name used in TopicModel.
	 * @throws IOException
	 */
	public static void processXml(String xmlFile, String outfile, String name) throws IOException {
		System.err.println("Processing '" + xmlFile + "'...");
		int id = 0;
		List<IndexDocument> docList = getDocsFromXml(xmlFile);
		FileWriter fw = new FileWriter(outfile);
		for (IndexDocument doc : docList) {
			// write each line while removing line breaks and unwanted characters
			fw.write(name + "-" + String.format("%06d", id) + "\tX\t" + doc.getContent().replaceAll("[\n\\x85\\u2028]", " ") + "\n");
			id++;
		}
		fw.close();
		System.err.println(id + " documents processed. Output saved as '" + outfile + "'");
	}
	
	/**
	 * Reads documents from an XML file and returns them as a list.
	 * Every document contains its title, abstract, content, publish date and name of the publisher
	 * 
	 * @param xmlFile
	 * @return
	 */
	public static List<IndexDocument> getDocsFromXml(String xmlFile) {

		List<IndexDocument> docList = new ArrayList<IndexDocument>();
		
		try {

			File fXmlFile = new File(xmlFile);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);

			// TODO: check if this is necessary
			// optional, but recommended
			// read this -
			// http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
			doc.getDocumentElement().normalize();

			NodeList docElements = doc.getElementsByTagName("doc");
			for (int i = 0; i < docElements.getLength(); i++) {
				
				String docTitle = "";
				String docAbstract = "";
				String docContent = "";
				String docPublishDate = "";
				String docPublisherName = "";
				
				Node docNode = docElements.item(i);
				
				if (docNode.getNodeType() == Node.ELEMENT_NODE) {

					Element docElement = (Element) docNode;

					NodeList docChildren = docElement.getChildNodes();
					
					for (int j = 0; j < docChildren.getLength(); j++) {

						Node docChildNode = docChildren.item(j);

						if (docChildNode.getNodeType() == Node.ELEMENT_NODE) {
							
							Element docChildElement = (Element) docChildNode;
							String attributeName = docChildElement.getAttribute("name");
							
							switch (attributeName) {
								case "title":
									docTitle = docChildElement.getElementsByTagName("str").item(0).getTextContent();
									break;
								case "abstract":
									docAbstract = docChildElement.getElementsByTagName("str").item(0).getTextContent();
									break;
								case "content_4hl":
									docContent = docChildElement.getElementsByTagName("str").item(0).getTextContent();
									break;
								case "publish_date":
									docPublishDate = docChildElement.getTextContent();
									break;
								case "publisher_name":
									docPublisherName = docChildElement.getTextContent();
									break;
							}
						}
					}

				}
				docList.add(new IndexDocument(docTitle, docAbstract, docContent, docPublishDate, docPublisherName));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return docList;

	}
	
	/**
	 * Stores a new data file that contains only documents which match a certain query.
	 * 
	 * @param csvFile Data file to clean.
	 * @param searchString Query to match.
	 * @param outfile New clean data file. Contains only documents which match {@code searchString}.
	 * @param outfileRemoved File that stores the documents which didn't match {@code searchString}.
	 * @throws Exception
	 */
	public static void removeNoHitDocsFromData(File csvFile, String searchString, File outfile, File outfileRemoved) throws Exception {
		System.err.println("Writing '" + outfile + " which only contains documents that match query '" + searchString + "'...");
		// index all files in csvFile
		Directory index = Indexer.buildRAMIndexFromCsv(csvFile, null);
		// search for searchString in index
		ResultDocument[] resultDocs = Searcher.search(index, searchString, Integer.MAX_VALUE, false, false, null);
		// remove docs / write "clean" CSV that contains only docs from search result
		Set<String> docsToStore = new HashSet<String>();
		for (ResultDocument resultDoc : resultDocs) {
			docsToStore.add(resultDoc.getDocID());
		}
		BufferedReader br = new BufferedReader(new FileReader(csvFile));
		FileWriter fw = new FileWriter(outfile);
		FileWriter fwRemoved = new FileWriter(outfileRemoved);
		String line;
		int curLine = 1;
		while ((line = br.readLine()) != null) {
			Pattern p = Pattern.compile("^(\\S*)[\\s,]*(\\S*)[\\s,]*(.*)$");
	    	Matcher m = p.matcher(line);
	    	if (!m.matches()) {
	    		System.out.println("Error at line " + curLine + " (no matches found): " + line);
			}
	    	// check correctness of relevance indicator
	    	String docId = m.group(1); @SuppressWarnings("unused") String label = m.group(2); @SuppressWarnings("unused") String content = m.group(3);
	    	// store document only if it is occurs in the search results
	    	if (docsToStore.contains(docId)) {
				fw.write(line + "\n");
			} else {
				fwRemoved.write(line + "\n");
			}
	    }
		br.close();
		fw.close();
		fwRemoved.close();
	}

	/**
	 * Shuffles lines in a document.
	 * From the shuffled file the first 100 (or more) documents can be used to manually build a goldstandard. 
	 * 
	 * @param dataFile File to be shuffled.
	 * @param outfile File location to store shuffled file.
	 * @throws IOException
	 */
	public static void shuffleProcessedData(String dataFile, String outfile) throws IOException {
		System.err.println("Shuffling '" + dataFile + "'...");
		
		// read lines from file
		List<String> lines = new ArrayList<String>();
		BufferedReader br = new BufferedReader(new FileReader(dataFile));
		String line;
		while ((line = br.readLine()) != null) {
			lines.add(line);
		}
		br.close();
		
		// shuffle
		Collections.shuffle(lines);
		
		// write shuffled lines to file
		FileWriter fw = new FileWriter(outfile);
		for (String l : lines) {
			// write each line while removing line breaks and unwanted characters
			fw.write(l + "\n");
		}
		fw.close();
		System.err.println("Done! Output saved as '" + outfile + "'");
	}

	/**
	 * Checks goldstandard file for some syntactic and semantic errors.
	 * <p>Some rules for a correct goldstandard:
	 * <ul>
	 * <li>Each line is of the form {@code docID – relevance judgment – highlight}, where "–" is a tab.</li>
	 * <li>The relevance judgment uses 'R' for relevant and 'N' for nonrelevant.</li>
	 * <li>For a relevant document: Every highlight has its own line. So a relevant document with 3 highlights builds 3 lines (one for each highlight).</li>
	 * <li>A relevant document must have at least one highlight.</li>
	 * <li>Multiple lines with same docIDs must occur in one contiguous block.</li>
	 * </ul></p>
	 * 
	 * @param goldstandardFile
	 * @return 
	 * @throws IOException
	 */
	public static int checkGoldstandard(String goldstandardFile) throws IOException {
		System.err.println("Checking '" + goldstandardFile + "'...");
		
		// read lines from file
		BufferedReader br = new BufferedReader(new FileReader(goldstandardFile));
		String line;
		int curLine = 1;
		int numberOfErrors = 0;
		Set<String> existingDocIDs = new HashSet<String>();
		String prevDocID = "";
		while ((line = br.readLine()) != null) {
			Pattern p = Pattern.compile("^(\\S*)[\\s,]*(\\S*)[\\s,]*(.*)$");
	    	Matcher m = p.matcher(line);
	    	if (!m.matches()) {
	    		System.out.println("Error at line " + curLine + " (no matches found): " + line);
			}
	    	// check correctness of relevance indicator
	    	String docId = m.group(1); String relevance = m.group(2); String highlight = m.group(3);
	    	if (!(relevance.equals("N") | relevance.equals("R"))) {
				System.out.println("Wrong relevance indicator (use 'N' or 'R') at line " + curLine + ": " + docId + "\t" + relevance + "\t" + highlight);
				numberOfErrors++;
			}
	    	// check if there's a highlight for a relevant document
	    	if (relevance.equals("R") && highlight.isEmpty()) {
	    		System.out.println("No highlight found for relevant document at line " + curLine + ": " + docId + "\t" + relevance + "\t" + highlight);
	    		numberOfErrors++;
			}
	    	// check whether docIDs form contiguous block (i.e. no docID is accidentally used again later)
	    	if (existingDocIDs.contains(docId) && !prevDocID.equals(docId)) {
				System.out.println("DocID error at line " + curLine + ": " + docId + "\t" + relevance + "\t" + highlight);
				numberOfErrors++;
			}
	    	existingDocIDs.add(docId);
	    	prevDocID = docId;
	    	curLine++;
		}
		br.close();
		if (numberOfErrors > 0) {
			System.err.println("Found " + numberOfErrors + " syntax errors. Please check your goldstandard.");
		}
		else {
			System.err.println("No syntax errors found. Your goldstandard seems to be ok.");
		}
		System.err.println("Your goldstandard contains " + existingDocIDs.size() + " instances.");
		return existingDocIDs.size();
	}
	
	/**
	 * Get total number of documents marked relevant in goldstandard. Only Documents between {@code goldstdStart} and {@code goldstdEnd} are counted.
	 * 
	 * @param goldstandard
	 * @param goldDocs
	 * @return
	 * @throws IOException
	 * @throws InvalidRelevanceException
	 */
	public static int getTotalRelevant(String goldstandard, Set<Integer> goldDocs) throws IOException, InvalidRelevanceException {
		BufferedReader br = new BufferedReader(new FileReader(new File(goldstandard)));
		Set<String> relevantDocs = new HashSet<String>();
		String line;
		int curLine = 1;
	    int curDoc = 0;
	    String prevDocId = "";
	    while ((line = br.readLine()) != null) {
	    	Pattern p = Pattern.compile("^(\\S*)[\\s,]*(\\S*)[\\s,]*(.*)$");
	    	Matcher m = p.matcher(line);
	    	if (!m.matches()) {
	    		System.out.println("Error at line " + curLine + " in goldstandard (no matches found): " + line);
			}
	    	String docId = m.group(1); String relevance = m.group(2); @SuppressWarnings("unused") String highlight = m.group(3);
	    	// a new document occurs only if docId changes
	    	if (!prevDocId.equals(docId)) {
				curDoc++;
				prevDocId = docId;
			}
	    	// count relevant documents only if they are in range (between goldstdStart and goldstdEnd)
	    	if (relevance.equals("R")) {
	    		if (goldDocs.contains(curDoc)) {
	    			relevantDocs.add(docId);
	    		}
			} else if (!relevance.equals("N")) {
				br.close();
				throw new InvalidRelevanceException();
			}
			curLine++;
	    }
	    br.close();
		return relevantDocs.size();
	}

	/**
	 * Get total number of documents marked relevant in goldstandard. Counts all relevant documents.
	 * 
	 * @param goldstandard
	 * @return
	 * @throws IOException
	 * @throws InvalidRelevanceException
	 */
	public static int getTotalRelevant(String goldstandard) throws IOException, InvalidRelevanceException {
		BufferedReader br = new BufferedReader(new FileReader(new File(goldstandard)));
		Set<String> relevantDocs = new HashSet<String>();
		String line;
		int curLine = 1;
	    String prevDocId = "";
	    while ((line = br.readLine()) != null) {
	    	Pattern p = Pattern.compile("^(\\S*)[\\s,]*(\\S*)[\\s,]*(.*)$");
	    	Matcher m = p.matcher(line);
	    	if (!m.matches()) {
	    		System.out.println("Error at line " + curLine + " in goldstandard (no matches found): " + line);
			}
	    	String docId = m.group(1); String relevance = m.group(2); @SuppressWarnings("unused") String highlight = m.group(3);
	    	// a new document occurs only if docId changes
	    	if (!prevDocId.equals(docId)) {
				prevDocId = docId;
			}
	    	// count relevant documents only if they are in range (between goldstdStart and goldstdEnd)
	    	if (relevance.equals("R")) {
	    		relevantDocs.add(docId);
			} else if (!relevance.equals("N")) {
				br.close();
				throw new InvalidRelevanceException();
			}
			curLine++;
	    }
	    br.close();
		return relevantDocs.size();
	}
	
	/**
	 * Calculates relevant documents that {@code goldstandard1} and {@code goldstandard2} have in common.
	 * 
	 * @param goldstandard1
	 * @param goldstandard2
	 * @return
	 * @throws IOException
	 * @throws InvalidRelevanceException
	 */
	public static int getOverlap(String goldstandard1, String goldstandard2) throws IOException, InvalidRelevanceException {
		Set<String> relevantDocsGs1 = extractRelevantDocs(goldstandard1);
		Set<String> relevantDocsGs2 = extractRelevantDocs(goldstandard2);
		int overlap = 0;
		for (String docID : relevantDocsGs1) {
			if (relevantDocsGs2.contains(docID)) {
				overlap++;
			}
		}
		return overlap;
	}

	private static Set<String> extractRelevantDocs(String goldstandard) throws IOException, InvalidRelevanceException {
		BufferedReader br = new BufferedReader(new FileReader(new File(goldstandard)));
		Set<String> relevantDocs = new HashSet<String>();
		String line;
		int curLine = 1;
	    String prevDocId = "";
	    while ((line = br.readLine()) != null) {
	    	Pattern p = Pattern.compile("^(\\S*)[\\s,]*(\\S*)[\\s,]*(.*)$");
	    	Matcher m = p.matcher(line);
	    	if (!m.matches()) {
	    		System.out.println("Error at line " + curLine + " in goldstandard (no matches found): " + line);
			}
	    	String docId = m.group(1); String relevance = m.group(2); @SuppressWarnings("unused") String highlight = m.group(3);
	    	// a new document occurs only if docId changes
	    	if (!prevDocId.equals(docId)) {
				prevDocId = docId;
			}
	    	// count relevant documents only if they are in range (between goldstdStart and goldstdEnd)
	    	if (relevance.equals("R")) {
	    		relevantDocs.add(docId);
			} else if (!relevance.equals("N")) {
				br.close();
				throw new InvalidRelevanceException();
			}
			curLine++;
	    }
	    br.close();
		return relevantDocs;
	}
	
	/**
	 * Creates list of documents for which a boost factor can be calculated. Documents contain docID and content.
	 * {@code goldstandard} is used to determine which documents to include in the list, {@code data} is used to
	 * get the content for those documents.
	 * <p>List contains relevant and nonrelevant documents.</p>
	 * 
	 * @param goldstandard Documents in {@code goldstandard} will be used to create assessable documents.
	 * Relevant and nonrelevant documents will be added to the list.
	 * @param docsToIndex Elements to index in goldstandard.
	 * @param data Needed to get content for relevant documents in {@code goldstandard}.
	 * @return List of relevant documents with their docID and content.
	 * @throws InvalidRelevanceException
	 * @throws IOException
	 */
	public static List<AssessableDocument> buildDocsToAssess(String goldstandard, Set<Integer> docsToIndex, String data) throws InvalidRelevanceException, IOException {
		
		List<AssessableDocument> assessableDocs = new ArrayList<AssessableDocument>();
		BufferedReader br = new BufferedReader(new FileReader(new File(goldstandard)));
		Set<String> docsToAdd = new HashSet<String>(); // contains docIDs of documents in goldstandard (that are between goldstdStart and goldstdEnd)
		String line;
		int curLine = 1;
		int curDoc = 0;
		String prevDocId = "";
	    while ((line = br.readLine()) != null) {
	    	Pattern p = Pattern.compile("^(\\S*)[\\s,]*(\\S*)[\\s,]*(.*)$");
	    	Matcher m = p.matcher(line);
	    	if (!m.matches()) {
	    		System.out.println("Error at line " + curLine + " in goldstandard (no matches found): " + line);
			}
	    	String docId = m.group(1); @SuppressWarnings("unused") String relevance = m.group(2); @SuppressWarnings("unused") String highlight = m.group(3);
	    	// a new document occurs only if docId changes
	    	if (!prevDocId.equals(docId)) {
				curDoc++;
				prevDocId = docId;
			}
			// only index document if it's in indexing range
	    	if (docsToIndex.contains(curDoc)) {
	    		docsToAdd.add(docId);
			}
			curLine++;
	    }
	    br.close();
	    br = new BufferedReader(new FileReader(new File(data)));
	    curLine = 1;
	    while ((line = br.readLine()) != null) {
	    	Pattern p = Pattern.compile("^(\\S*)[\\s,]*(\\S*)[\\s,]*(.*)$");
	    	Matcher m = p.matcher(line);
	    	if (!m.matches()) {
	    		System.out.println("Error at line " + curLine + " in data file (no matches found): " + line);
			}
	    	String docId = m.group(1); @SuppressWarnings("unused") String label = m.group(2); String content = m.group(3);
	    	if (docsToAdd.contains(docId)) {
	    		assessableDocs.add(new AssessableDocument(docId, content));
			}
			curLine++;
	    }
	    br.close();
	    System.err.println("Created " + assessableDocs.size() + " assessable documents.");
		return assessableDocs;
	}

	/**
		 * Creates relevance feedback as a list of judgments (judged documents). Judgments do NOT contain any highlights!
		 * Use {@code buildRelevanceFeedbackWithHighlights} to get judgments with highlights.
		 * {@code goldstandard} is used to determine which documents to include in the list, {@code data} is used to
		 * get the content for those documents.
		 * <p>Contains only relevant documents!</p>
		 * 
		 * @param goldstandard Goldstandard from which the relevance feedback is built.
		 * Nonrelevant documents will not be added to the list.
		 * @param docsForRF Documents in goldstandard to use for relevance feedback.
		 * @param data Needed to get content for relevant documents in {@code goldstandard}.
		 * @return List of relevant documents with their docID and highlighted text passages.
		 * @throws InvalidRelevanceException
		 * @throws IOException
		 */
		public static List<JudgedDocument> buildRelevanceFeedbackWithContents(String goldstandard, Set<Integer> docsForRF, String data) throws InvalidRelevanceException, IOException {
			
			List<JudgedDocument> relevanceFeedbackNoHighlights = new ArrayList<JudgedDocument>();
			BufferedReader br = new BufferedReader(new FileReader(new File(goldstandard)));
			Set<String> relevantDocs = new HashSet<String>(); // contains docIDs of documents marked relevant in goldstandard (that are between goldstdStart and goldstdEnd)
			String line;
			int curLine = 1;
			int curDoc = 0;
		    String prevDocId = "";
		    while ((line = br.readLine()) != null) {
		    	Pattern p = Pattern.compile("^(\\S*)[\\s,]*(\\S*)[\\s,]*(.*)$");
		    	Matcher m = p.matcher(line);
		    	if (!m.matches()) {
		    		System.out.println("Error at line " + curLine + " in goldstandard (no matches found): " + line);
				}
		    	String docId = m.group(1); String relevance = m.group(2); @SuppressWarnings("unused") String highlight = m.group(3);
		    	// a new document occurs only if docId changes
		    	if (!prevDocId.equals(docId)) {
					curDoc++;
					prevDocId = docId;
				}
		    	// only add document to relevance feedback if it is relevant and in range (between goldstdStart and goldstdEnd)
		    	if (relevance.equals("R")) {
		    		if (docsForRF.contains(curDoc)) {
		    			relevantDocs.add(docId);
		    		}
				} else if (!relevance.equals("N")) {
					br.close();
					throw new InvalidRelevanceException();
				}
				curLine++;
		    }
		    br.close();
		    br = new BufferedReader(new FileReader(new File(data)));
		    curLine = 1;
		    while ((line = br.readLine()) != null) {
		    	Pattern p = Pattern.compile("^(\\S*)[\\s,]*(\\S*)[\\s,]*(.*)$");
		    	Matcher m = p.matcher(line);
		    	if (!m.matches()) {
		    		System.out.println("Error at line " + curLine + " in data file (no matches found): " + line);
				}
		    	String docId = m.group(1); @SuppressWarnings("unused") String label = m.group(2); String content = m.group(3);
		    	if (relevantDocs.contains(docId)) {
					relevanceFeedbackNoHighlights.add(new JudgedDocument(docId, true, content, null));
	//    			System.out.println(docId);
				}
				curLine++;
		    }
		    br.close();
		    System.err.println("Built relevance feedback (no highlights / only content) of size " + relevanceFeedbackNoHighlights.size() + ".");
			return relevanceFeedbackNoHighlights;
		}

	/**
		 * Creates relevance feedback as a list of judgments (judged documents). Judgments do NOT contain any content!
		 * Use {@code buildRelevanceFeedbackWithContents} to get judgments with content.
		 * <p>Contains only relevant documents!</p>
		 * 
		 * @param goldstandard Goldstandard from which the relevance feedback is built. Documents with same docID must appear together (line after line)!
		 * @param docsForRF Documents in goldstandard to use for relvance feedback.
		 * @return List of relevant documents with their docID and highlighted text passages.
		 * @throws InvalidRelevanceException
		 * @throws IOException
		 */
		public static List<JudgedDocument> buildRelevanceFeedbackWithHighlights(String goldstandard, Set<Integer> docsForRF) throws InvalidRelevanceException, IOException {
			
			List<JudgedDocument> relevanceFeedbackNoContent = new ArrayList<JudgedDocument>();
			BufferedReader br = new BufferedReader(new FileReader(new File(goldstandard)));
			String line;
			int curLine = 1;
			int curDoc = 0;
			String prevDocId = "";
			boolean isFirstLineOfDoc = true;
		    while ((line = br.readLine()) != null) {
		    	Pattern p = Pattern.compile("^(\\S*)[\\s,]*(\\S*)[\\s,]*(.*)$");
		    	Matcher m = p.matcher(line);
		    	if (!m.matches()) {
		    		System.out.println("Error at line " + curLine + " in goldstandard (no matches found): " + line);
				}
		    	String docId = m.group(1); String relevance = m.group(2); String highlight = m.group(3);
		    	// a new document occurs only if docId changes
		    	if (!prevDocId.equals(docId)) {
					curDoc++;
					prevDocId = docId;
					isFirstLineOfDoc = true;
				} else {
					isFirstLineOfDoc = false;
				}
		    	// only add document to relevance feedback if it is relevant and in range (between goldstdStart and goldstdEnd)
		    	if (relevance.equals("R")) {
		    		if (docsForRF.contains(curDoc)) {
		    			// create new document
		    			if (isFirstLineOfDoc) {
		    				List<String> highlights = new ArrayList<String>();
							highlights.add(highlight);
							JudgedDocument newDoc = new JudgedDocument(docId, true, null, highlights);
							relevanceFeedbackNoContent.add(newDoc);
	//						System.out.println(docId);
						// add highlight to the same document
						} else {
							int lastElement = relevanceFeedbackNoContent.size() - 1;
							JudgedDocument prevDoc = relevanceFeedbackNoContent.get(lastElement);
							// check if we are adding highlight to the right document
							if (!docId.equals(prevDoc.getDocID())) {
								br.close();
								throw new IOException("DocIDs do not match!");
							}
							prevDoc.getHighlights().add(highlight);
							relevanceFeedbackNoContent.set(lastElement, prevDoc);
						}
		    		}
				} else if (!relevance.equals("N")) {
					br.close();
					throw new InvalidRelevanceException();
				}
				curLine++;
		    }
		    br.close();
		    System.err.println("Built relevance feedback (no content / only highlights) of size " + relevanceFeedbackNoContent.size() + ".");
			return relevanceFeedbackNoContent;
		}

	/**
	 * Creates a map of relevance judgments. Key = docID, value = boolean for relevance ({@code true} for relevant, {@code false} for nonrelevant).
	 * 
	 * @param gold
	 * @param goldDocs
	 * @return
	 * @throws InvalidRelevanceException
	 * @throws IOException
	 */
	public static Map<String, Boolean> getRelevanceJudgements(String gold, Set<Integer> goldDocs) throws InvalidRelevanceException, IOException {
			Map<String, Boolean> judgements = new HashMap<String, Boolean>();
			BufferedReader br = new BufferedReader(new FileReader(gold));
			String line;
			int curLine = 1;
			int curDoc = 0;
			String prevDocId = "";
		    while ((line = br.readLine()) != null) {
		    	Pattern p = Pattern.compile("^(\\S*)[\\s,]*(\\S*)[\\s,]*(.*)$");
		    	Matcher m = p.matcher(line);
		    	if (!m.matches()) {
		    		System.out.println("Error at line " + curLine + " in goldstandard (no matches found): " + line);
				}
		    	String docId = m.group(1); String relevance = m.group(2); @SuppressWarnings("unused") String highlight = m.group(3);
		    	// a new document occurs only if docId changes
		    	if (!prevDocId.equals(docId)) {
					curDoc++;
					prevDocId = docId;
				}
				Boolean relevanceBoolean = null;
		    	if (relevance.equals("R")) {
					relevanceBoolean = new Boolean(true);
				} else if (relevance.equals("N")) {
					relevanceBoolean = new Boolean(false);
				} else {
					br.close();
					throw new InvalidRelevanceException();
				}
		    	// only add document to relevance judgment if it is in indexing range
		    	if (goldDocs.contains(curDoc)) {
					judgements.put(docId, relevanceBoolean);
				}
		    	curLine++;
		    }
		    br.close();
	//	    System.err.println("Evaluating with relevance judgment of size " + judgements.size() + ".");
			return judgements;
		}

	/**
	 * Helper method to inspect strange characters and their encoding.
	 * 
	 * @param file File to read.
	 * @param lineNumber Line number to inspect.
	 * @param Position to start looking.
	 * @param end Position to stop looking.
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public static void printUnicodeChars(String file, int lineNumber, int start, int end)
			throws IOException, FileNotFoundException {
		String line;
		try (InputStream fis = new FileInputStream(file);
				InputStreamReader isr = new InputStreamReader(fis, Charset.forName("UTF-8"));
				BufferedReader br = new BufferedReader(isr);) {
			int lineNr = 1;
			while ((line = br.readLine()) != null) {
				if (lineNr == lineNumber) {
					String ominousString = line.substring(start, end);
					for (char c : ominousString.toCharArray()) {
						System.out.println(c + "\t" + (int) c);
					}
				}
				lineNr++;
			}
		}
	}

}
