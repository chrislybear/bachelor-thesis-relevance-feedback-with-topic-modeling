package ch.uzh.data;

public class IndexDocument {

	private String title;
	private String abstrct;
	private String content;
	private String publishDate;
	private String publisherName;
	
	public IndexDocument(String title, String abstrct, String content, String publishDate, String publisherName) {
		this.title = title;
		this.abstrct = abstrct;
		this.content = content;
		this.publishDate = publishDate;
		this.publisherName = publisherName;
	}
	
	public String getTitle() {
		return title;
	}
	public String getAbstract() {
		return abstrct;
	}
	public String getContent() {
		return content;
	}
	public String getPublishDate() {
		return publishDate;
	}
	public String getPublisherName() {
		return publisherName;
	}
	
}
