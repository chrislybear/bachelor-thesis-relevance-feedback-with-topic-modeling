package ch.uzh.data;

public class ResultDocument {

	private String docID;
	private float score;
	
	public ResultDocument(String docID, float score) {
		this.docID = docID;
		this.score = score;
	}

	public String getDocID() {
		return docID;
	}

	public float getScore() {
		return score;
	}
}
