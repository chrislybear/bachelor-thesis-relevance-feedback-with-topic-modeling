# Bachelor Thesis: Relevance Feedback with Topic Modeling #
Christoph Schwizer, July 20, 2016

[Download Thesis](https://bitbucket.org/chrislybear/bachelor-thesis-relevance-feedback-with-topic-modeling/raw/aac76acbeffa8db32596e46d8238f5f04ba2b0a3/Relevance%20Feedback%20with%20Topic%20Modeling.pdf)

### How to setup development environment ###

* Necessary .jar files for Lucene (5.5.0) and Mallet (2.0.8RC3) are included. They can also be downloaded from http://lucene.apache.org and http://mallet.cs.umass.edu/download.php
* Make sure the .jar files in the /lib directory are added to the build path (in Eclipse: right click on jar → "Build Path" → "Add to Build Path"
* To also include javadoc for the Lucene library in Eclipse, go to build path configuration of your project (right click on project in Package explorer → "Build Path" → "Configure Build Path..."), then for each .jar open its dropdown menu, select "Javadoc location", then "Edit...". You can then redirect directly to the Lucene online documentation (e.g. for lucene-core-5.5.0.jar the location would be "http://lucene.apache.org/core/5_5_0/core/").
   * Lucene Documentation: http://lucene.apache.org/core/5_5_0/
   * Mallet Documentation: http://mallet.cs.umass.edu/api/